# CHANGELOG
All important changes in this project are stored in this file.

[Semantic versioning](https://semver.org/spec/v2.0.0.html) is used.

## Unreleased - in development

### Breaking

### Added

### Changed

### Deprecated

### Removed

### Fixed

- Local config files are now correctly loaded again.
- GFI config is no longer showing duplicate attributes

## v1.0.2 - 2024-06-27

### Breaking

### Added

- new input array representation as a table
- InputObject shows a warning sign with tooltip when a required child property is empty
- InputObject is colored red when a required child property is empty
- the "delete local data" button is now triggering a confirmation dialog before clearing any data
- the "delete local data" button is now also available in production environment
- Allow configuration of chapters via `excludedChapters`.
- Users can get a quick preview from Portalconfig/Themenconfig.
- Name and legend of Layerconfig's layers may now be configured.
- Group layers are labeled with GROUP in layer lists now.
- Allow modifying layer names in Themenconfig.
- Allow reordering object entries that care for ordering.
  As config.json.md does not (machine-readable) contains that information, the configuration key `isSortable` was added.
- All Themenconfig layers may be removed simultaneously.
- Invalid layers (e.g., for incompatible services.json) are detected, warned and removed from Themenconfig.
- Displayed feature attributes (`gfiAttributes` key) are configurable for Themenconfig layers.
- It is now possible to upload layerConfig, restConfig and styleConfig from local computer.
- Renaming may be done by plain text or by configuration key now.
  This allows for different labels, e.g., all `children` elements, to be named different.

### Changed

- text inputs: required fields with a default value are no longer showing a requirement error
- text inputs: available placeholders are always shown as long as the input value is empty
- Group layers cannot contain other group layers (or itself) any longer, as this is not supported by Masterportal.
- Object blocks in Portalconfig are collapsed by default now.
- Improved drag and drop support for treeType custom.
- Folders are only automatically opened if they contain three or less layers or at least one folder.
- Folders in treeType custom can now be moved into other folders and changed in hierarchy.
- Configuration reload needs to be confirmed explicitly now.
- config.masterportalApi.json's default layer is changed from 6357 to 23420 (Basemap).
- Updated dependencies

### Deprecated

### Removed

### Fixed

- MPADMIN-170: Input type array with nested objects will be rendered correctly now
- Warnings about incompatible versions of hosted Masterportal are no longer shown if versions match.
- Export correct manually overridden paths for 3J configs to pre-configured Masterportal.
- Invalid Layerconfig URLs are detected and don't crash the Admintool any longer.
- Path handling for production and installations without node-sspi work on Windows now.
- If treeType is light initially, it is ensured all configured layers are visible in Themenconfig now.
- Missing layer names are now taken from layerConf and displayed in Themenconfig.
- pem "RSA PRIVATE KEY" missing issues fixed upstream.


## v1.0.1 - 2022-10-21

### Breaking

### Added

- It is now possible to load layerConfig, restConfig and styleConfig via url
- Added the MapElement to substitute TheMap by inline positioned maps for different inputs.
- Set the default target version to the version of the hosted Masterportal which is used for preview / publish.
- Path to sqlite database is configurable. Therefore the database can be easily stored within a persistent volume (docker).
- Display warning if target version and version of hosted Masterportal mismatch.
- Added layer dropdown with a list of all layers
- Added WMTS-Layer
- Themenconfig supports treeType custom

### Changed

- Style config now uses dropdown component
- Don't restrict names for published instances of Masterprortal to a certain scheme
- Added an indicator for dragging items of the layer lists
- Updated the Masterportal-Api to 2.3.0
- The menu titles aren't grey anymore before navigating
- Changed a few locales to be more self-explaining
- Edited documentation to add naming conventions for components.
- Changed the marker of the start coordinate to crosshairs for better visibility
- Renamed config.js and config.json into config.admintool.js, config.admintool.json
- Renamed mapConfig.js and mapConfig.json into config.masterportalApi.js, config.masterportalApi.json
- Translated Portal-Config-Menu-Tree in config.json.md into English
- Restrict style configuration to styles that fit the given WFS layer
- Allow search result update in Themenconfig by pressing enter
- Updated several dependencies
- Endpoints for cleanByAge and cleanByID now return HTTP status 204 instead of text 'ok'
- Migration to Vue 3
- Refactoring of most components
- Building via recommended Vite instead of webpack
- The recommended docker setup does not clone the repository within Docker anymore.
  The legacy `Dockerfile` (for the moment, available as `Dockerfile.git`) is deprecated.
  It has a lot of drawbacks, especially, source code change detection and useful caching does not work.


### Deprecated

### Removed

### Fixed

- JSON code gets displayed appropriate
- Fixed layer list to reset position, if search parameter changes
- Handling of `basePath` in `app/config.json` works correctly now
- Removal of layers in services-internet.json will be applied
- Attributes with multiple supported types work better now

## v1.0.0 - 2021-09-23

- The Admintool is now available in German and English. Since huge parts of it rely on the `config.json.md` and `config.json.de.md` of the *Masterportal*, this is a breaking change. These files are only available from version `2.7.0` onwards; older versions of the *Masterportal* are no longer supported.
- The initial localization may be chosen by using a query parameter, e.g. `?language=en`. It defaults to `de`.

## v0.0.1 - 2021-01-07

- Initial release.
