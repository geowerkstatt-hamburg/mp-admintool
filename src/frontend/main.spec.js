import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';

import { createApp } from 'vue';
vi.mock('vue', () => ({
	createApp: vi.fn(),
}));

vi.mock('@/fonts.scss', '');
vi.mock('@/App.vue', () => ({
	default: 'TEST-APP',
}));
vi.mock('@/router', () => ({
	default: () => 'TEST-ROUTER',
}));
vi.mock('@/plugins/vuetify', () => ({
	default: 'TEST-VUETIFY',
}));
vi.mock('@/plugins/generic-components', () => ({
	default: 'TEST-GC',
}));
vi.mock('@/plugins/pinia', () => ({
	default: 'TEST-PINIA',
}));
vi.mock('@/plugins/i18n', () => ({
	default: 'TEST-I18N',
}));

describe('main', () => {
	beforeEach(ctx => {
		createApp.mockReturnValue(ctx.app = {
			use: vi.fn(),
			mount: vi.fn(),
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('triggers all vue-related initialization methods', async ({ app }) => {
		await import('@/main');
		expect(createApp).toHaveBeenCalledWith('TEST-APP');
		expect(app.use).toHaveBeenCalledWith('TEST-VUETIFY');
		expect(app.use).toHaveBeenCalledWith('TEST-GC');
		expect(app.use).toHaveBeenCalledWith('TEST-PINIA');
		expect(app.use).toHaveBeenCalledWith('TEST-I18N');
		expect(app.use).toHaveBeenCalledWith('TEST-ROUTER');
		expect(app.mount).toHaveBeenCalledWith('#app');
	});
});
