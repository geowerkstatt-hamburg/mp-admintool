import { describe, it, expect, beforeAll } from 'vitest';

import englishTranslation from '@/locales/en';
import germanTranslation from '@/locales/de';
const translations = {
	en: englishTranslation,
	de: germanTranslation,
};

let deepKeys;
function getDeepKeys (obj) {
	return typeof obj === 'object' ? Object.entries(obj).flatMap(([ key, value ]) => [ key, ...getDeepKeys(value).map(sub => `${key}.${sub}`) ]) : [];
}

describe('locales', () => {
	beforeAll(() => {
		deepKeys = getDeepKeys(englishTranslation);
	});

	describe.each([
		[ 'de' ],
		[ 'en' ],
	])('%s', key => {
		it('is object and contains chapters key', () => {
			expect(translations[key]).toBeTypeOf('object');
			expect(translations[key].chapters).toBeTypeOf('object');
		});
		it('contains the correct keys', () => {
			expect(getDeepKeys(translations[key]).sort()).toEqual(deepKeys.sort());
		});
		it.skip('contains the correct keys in equal order', () => {
			expect(getDeepKeys(translations[key])).toEqual(deepKeys);
		});
	});
});
