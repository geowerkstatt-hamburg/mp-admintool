import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { inject, nextTick } from 'vue';
import { render } from '@/test/components/helper';

import App from '@/App.vue';

import { useRoute } from 'vue-router';
import { useConfigStore } from '@/store/config';
import { useLayersStore } from '@/store/layers';

// Needed for mocking out OL library
vi.mock('@/lib/showMap', () => ({}));

describe('App', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		useLayersStore.mockReturnValue(ctx.layers = {
			initializeDB: vi.fn(),
		});
		useConfigStore.mockReturnValue(ctx.config = {
			initialize: vi.fn(),
		});
		useRoute().name = 'test';
		useRoute().meta.title = 'TEST';
		useRoute().meta.showNavButtons = true;
		ctx.render = render(App, {
			stubs: {
				TheMap: true,
				TheStorageDialog: true,
				TheLoadingIndicator: true,
				TheLocaleSwitch: true,
				TheHeader: true,
				TheNavigation: true,
				TheExpertCheckbox: true,
				TheNavigationButtons: true,
				TheAlert: true,
				TheFooter: true,
				TheBody: {
					template: '<slot />',
				},
				'router-view': {
					setup () {
						ctx.title = inject('title');
					},
					template: 'ROUTER',
				},
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('initializes correctly', ({ layers, config }) => {
		expect(layers.initializeDB).toHaveBeenCalled();
		expect(config.initialize).toHaveBeenCalled();
	});
	it('renders and updates title correctly', async ctx => {
		ctx.render.getByText('{$t(TEST)}');

		useRoute().name = 'update';
		useRoute().meta.title = 'UPDATE';
		await nextTick();
		ctx.render.getByText('{$t(UPDATE)}');

		ctx.title.value = 'DYNAMIC';
		await nextTick();
		ctx.render.getByText('{$t(DYNAMIC)}');
	});
});
