import { describe, beforeEach, afterEach, it, expect, vi } from 'vitest';
import { setActivePinia, createPinia } from 'pinia';
import { useConfigStore } from '@/store/config';

import axios from 'axios';
const masterportalTagListUrl = 'https://bitbucket.org/!api/2.0/repositories/geowerkstatt-hamburg/masterportal/refs/tags?pagelen=100&q=name~%22%22&sort=-name';
const masterportalRepositoryUrlPrefix = 'https://bitbucket.org/geowerkstatt-hamburg/masterportal/raw/';
let urlMap = {};
vi.mock('axios', () => ({
	default: {
		get: vi.fn(url => Promise.resolve({
			data: urlMap[url],
		})),
	},
}));

const layersLoadFn = vi.fn();
vi.mock('@/store/layers', () => ({
	useLayersStore: vi.fn(() => ({
		loadLayers: layersLoadFn,
	})),
}));

vi.mock('@/store/graph', () => ({
	useGraphStore: vi.fn(),
}));

import { useLoadingStore } from '@/store/loading';
vi.mock('@/store/loading', () => ({
	useLoadingStore: vi.fn(),
}));

const alertErrorFn = vi.fn();
vi.mock('@/store/alert', () => ({
	useAlertStore: vi.fn(() => ({
		error: alertErrorFn,
	})),
}));

vi.mock('@/lib/basePath', () => ({
	getBasePath: vi.fn(() => '/test'),
}));

vi.mock('@/lib/devModeFlag', () => ({
	devModeFlag: true,
	useLocalConfig: false,
}));

vi.mock('@/lib/filterMasterportalTags', () => ({
	default: vi.fn(tags => tags),
}));

vi.mock('@/lib/getHostedMasterportalVersion', () => ({
	getHostedMasterportalVersion: vi.fn(() => Promise.resolve({
		major: 42,
		minor: 0,
		patch: 0,
	})),
}));

vi.mock('@/lib/validators', () => ({
	isValidUrl: vi.fn(() => true),
}));

import { crs } from '@masterportal/masterportalapi';
vi.mock('@masterportal/masterportalapi', () => ({
	crs: {
		registerProjections: vi.fn(),
	},
}));

describe('config', () => {
	beforeEach(ctx => {
		setActivePinia(createPinia());
		useLoadingStore.mockReturnValue({
			register: ctx.loadingRegister = vi.fn(),
			unregister: ctx.loadingUnregister = vi.fn(),
		});
		urlMap = {
			[masterportalTagListUrl]: {
				values: [
					{ name: 'stable' },
				],
			},
			[`${masterportalRepositoryUrlPrefix}42/portal/basic/config.json`]: {
				master: 'portal',
			},
			[`${masterportalRepositoryUrlPrefix}42/doc/config.json.md`]: Symbol.for('PlaceholderDocumentation'),
			[`${masterportalRepositoryUrlPrefix}42/doc/config.json.de.md`]: Symbol.for('PlaceholderDocumentation'),
			'/test/app/config.admintool.json': {
				layerConf: '%LAYER%',
				restConf: '%REST%',
				styleConf: '%STYLE%',
				admin: true,
				excludedFeatures: [ 'foo' ],
			},
			'/test/app/config.masterportalApi.json': {
				namedProjections: [
					{ projection: true },
				],
			},
			'%LAYER%': Symbol.for('LAYER'),
			'%REST%': [
				{ rest: true },
			],
			'%STYLE%': [
				{ style: true },
			],
		};
		ctx.store = useConfigStore();
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	describe('initialize', () => {
		it('fetches configurations', async ({ store, loadingRegister, loadingUnregister }) => {
			const promise = store.initialize();
			expect(loadingRegister).toHaveBeenCalled();
			await promise;
			expect(store.hostedMasterportalVersion).toHaveProperty('major', 42);
			expect(store.masterportalApiConfig.namedProjections).toContainEqual({ projection: true });
			expect(crs.registerProjections).toHaveBeenCalled();
			expect(store.admintoolConfig).toHaveProperty('admin', true);
			expect(layersLoadFn).toHaveBeenCalledWith(Symbol.for('LAYER'));
			expect(store.restConf).toContainEqual({ rest: true });
			expect(store.styleConf).toContainEqual({ style: true });
			expect(store.masterportalTags).toEqual([
				{ name: 'dev' },
				{ name: 'stable' },
			]);
			expect(loadingUnregister).toHaveBeenCalled();
		});
		it('stops loading animation on network failure and does not crash', async ({ store }) => {
			const errorSpy = vi.spyOn(console, 'error').mockImplementation(() => { /**/ });
			axios.get.mockImplementation(() => {
				throw new Error('Mocked network failure');
			});
			await store.initialize();
			expect(errorSpy).toHaveBeenCalled();
		});
	});
	describe('onInitialized', () => {
		it('runs callback when initialization is finished', async ({ store }) => {
			const promise = new Promise(resolve => {
				store.onInitialized(resolve);
			});
			store.initialized = true;
			await promise;
		});
	});
	describe('isExcludedFeature', () => {
		it('returns true, if given key is in the excludedFeatures list', async ({ store }) => {
			const testFeature = 'foo';
			await store.initialize();
			const isExcluded = store.isExcludedFeature(testFeature);
			expect(isExcluded).toBe(true);
		});

		it('returns false, if given key is not in the excludedFeatures list', async ({ store }) => {
			const testFeature = 'bar';
			await store.initialize();
			const isExcluded = store.isExcludedFeature(testFeature);
			expect(isExcluded).toBe(false);
		});

		it('ignores case of key', async ({ store }) => {
			const testFeature = 'FoO';
			await store.initialize();
			const isExcluded = store.isExcludedFeature(testFeature);
			expect(isExcluded).toBe(true);
		});
	});
});
