import { describe, beforeEach, it, expect } from 'vitest';
import { setActivePinia, createPinia } from 'pinia';
import { useLoadingStore } from '@/store/loading';

describe('loading', () => {
	beforeEach(ctx => {
		setActivePinia(createPinia());
		ctx.store = useLoadingStore();
	});
	it('renders loading while promise is registered', ({ store }) => {
		expect(store.loading).toBe(false);
		store.register();
		expect(store.loading).toBe(true);
		store.unregister();
		expect(store.loading).toBe(false);
	});
	it('supports multiple loading modules in parallel', ({ store }) => {
		expect(store.loading).toBe(false);
		store.register(); // 1
		expect(store.loading).toBe(true);
		store.register(); // 2
		expect(store.loading).toBe(true);
		store.unregister(); // 1
		expect(store.loading).toBe(true);
		store.register(); // 2
		expect(store.loading).toBe(true);
		store.unregister(); // 1
		expect(store.loading).toBe(true);
		store.unregister(); // 0
		expect(store.loading).toBe(false);
	});
});
