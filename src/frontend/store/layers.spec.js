import { describe, afterEach, it, expect, vi, beforeAll } from 'vitest';
import { setActivePinia, createPinia } from 'pinia';
import { useLayersStore } from '@/store/layers';

import goodServices from './layers/goodServices';
import badServices from './layers/badServices';
import someServices from './layers/someServices';

async function check (db, conditions) {
	const layers = await db.lightLayerList.toArray();
	expect(layers.every(({ typ }) => [ 'WMS', 'WFS', 'WMTS' ].includes(typ))).toBe(true);
	Object.entries(conditions).forEach(([ matchTyp, count ]) => {
		expect(layers.filter(({ typ }) => matchTyp === typ).length).toBe(count);
	});
}
let db = null;
let store = null;

describe('layers', () => {
	beforeAll(() => {
		setActivePinia(createPinia());
		store = useLayersStore();
		store.initializeDB();
		({ db } = store.dbContainer);
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('leaves a clean database on empty re-initialization', async () => {
		await store.loadLayers([
			{ id: '1F', name: 'I bims, 1 WFS Layer', typ: 'WFS' },
			{ id: '1M', name: 'WMS ist auch da', typ: 'WMS' },
			{ id: '1U', name: 'Spannend, was es noch für Typen gibt', typ: 'MASTERPORTALAPI_ABSOLUTELY_UNSUPPORTED_LAYER_TYPE' },
		]);
		expect(await db.rawLayerList.count()).toEqual(3);
		expect(await db.lightLayerList.count()).toEqual(2);

		await store.loadLayers([]);
		expect(await db.rawLayerList.count()).toEqual(0);
		expect(await db.lightLayerList.count()).toEqual(0);
	});
	it('stores something in the db I want', async () => {
		await store.loadLayers(goodServices);
		await check(db, {
			'WMS': 2,
			'WFS': 2,
			'WMTS': 3,
		});
	});
	it('stores nothing in the db I do not want', async () => {
		await store.loadLayers(badServices);
		await check(db, {
			'WMS': 0,
			'WFS': 0,
			'WMTS': 0,
		});
	});
	it('filters layers and stores only someting in the db I want', async () => {
		await store.loadLayers(someServices);
		await check(db, {
			'WMS': 3,
			'WFS': 1,
			'WMTS': 3,
		});
	});
});
