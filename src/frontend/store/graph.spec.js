import { describe, beforeEach, afterEach, it, expect, vi } from 'vitest';
import { setActivePinia, createPinia } from 'pinia';
import { useGraphStore } from '@/store/graph';

vi.mock('@/store/config', () => ({
	useConfigStore: vi.fn(() => ({
		admintoolConfig: {},
	})),
}));

const loadingRegister = vi.fn();
const loadingUnregister = vi.fn();
vi.mock('@/store/loading', () => ({
	useLoadingStore: vi.fn(() => ({
		register: loadingRegister,
		unregister: loadingUnregister,
	})),
}));

vi.mock('@/store/alert', () => ({
	useAlertStore: vi.fn(() => ({
		error: vi.fn(),
	})),
}));

vi.mock('@/lib/renameHelper', () => ({
	renameLabels: vi.fn(),
}));

vi.mock('@/lib/devModeFlag', () => ({
	useLocalConfig: vi.fn(),
}));

vi.mock('@/lib/deepCopy', () => ({
	default: vi.fn(target => ({ deep: target })),
}));

vi.mock('@masterportal/mpconfigparser', () => ({
	parseMarkdown: () => Symbol.for('PlaceholderDocumentation'),
}));

vi.mock('i18next-vue', () => ({
	useTranslation: vi.fn(() => ({
		i18next: {
			language: 'de',
		},
	})),
}));

import axios from 'axios';
vi.mock('axios', () => ({
	default: {
		get: vi.fn(url => Promise.resolve({ data: (() => {
			if (url.endsWith('.json')) return { key: Symbol.for('defaultConfig') };
			if (url.endsWith('.json.md')) return Symbol.for('documentation');
			if (url.endsWith('.json.de.md')) return Symbol.for('documentationDe');
			throw new Error('Mocked network failure');
		})() })),
	},
}));

describe('graph', () => {
	beforeEach(ctx => {
		setActivePinia(createPinia());
		ctx.store = useGraphStore();
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});

	describe('fetchFromMasterportalVersion', () => {
		it('writes default config, working config, and markdown graph to store', async ({ store }) => {
			const promise = store.fetchFromMasterportalVersion({ version: 42, config: null });
			expect(loadingRegister).toHaveBeenCalled();
			await promise;
			expect(store.defaultConfig).toHaveProperty('key', Symbol.for('defaultConfig'));
			expect(store.config.deep).toHaveProperty('key', Symbol.for('defaultConfig'));
			expect(store.graph).toBe(Symbol.for('PlaceholderDocumentation'));
			expect(loadingUnregister).toHaveBeenCalled();
		});
		it('stops loading animation on network failure and does not crash', async ({ store }) => {
			const errorSpy = vi.spyOn(console, 'error').mockImplementation(() => { /**/ });
			axios.get.mockImplementation(() => {
				throw new Error('Mocked network failure');
			});
			await store.fetchFromMasterportalVersion({ version: 42, config: null });
			expect(errorSpy).toHaveBeenCalled();
			expect(loadingUnregister).toHaveBeenCalled();
		});
		it('copies the config, if any is given', async ({ store }) => {
			await store.fetchFromMasterportalVersion({ version: 42, config: { master: 'local' } });
			expect(store.defaultConfig).toHaveProperty('key', Symbol.for('defaultConfig'));
			expect(store.config).toHaveProperty('master', 'local');
		});
	});
});
