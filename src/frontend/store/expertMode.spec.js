import { describe, beforeEach, it, expect } from 'vitest';
import { setActivePinia, createPinia } from 'pinia';
import { useExpertModeStore } from '@/store/expertMode';

describe('expertMode', () => {
	beforeEach(context => {
		setActivePinia(createPinia());
		context.store = useExpertModeStore();
	});
	describe.each([
		[ false ],
		[ true ],
	])('sets expert mode to %s', value => {
		it('is successful', ({ store }) => {
			store.setExpertMode(value);
			expect(store.expertMode).toBe(value);
		});
	});
});
