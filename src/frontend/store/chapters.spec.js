import { describe, beforeEach, afterEach, it, expect, vi } from 'vitest';
import { setActivePinia, createPinia } from 'pinia';
import { useChaptersStore } from '@/store/chapters';

vi.mock('@/store/expertMode', () => ({
	useExpertModeStore: vi.fn(() => ({
		expertMode: false,
	})),
}));

import { useGraphStore } from '@/store/graph';
vi.mock('@/store/graph', () => ({
	useGraphStore: vi.fn(() => ({
		graph: {
			nodes: {
				Portalconfig: {
					chapterKey: 'Portalconfig',
					h: 2,
					label: 'foo',
					name: 'Portalconfig',
					path: [],
					outgoing: {
						menu: {
							name: 'menu',
							types: [
								[
									Object,
									'Portalconfig.menu',
								],
							],
						},
						searchBar: {
							name: 'searchBar',
							types: [
								[
									Object,
									'Portalconfig.searchBar',
								],
							],
						},
					},
				},
				'Portalconfig.menu': {
					chapterKey: 'Portalconfig.menu',
					h: 3,
					name: 'menu',
					label: 'menu',
					path: [ 'Portalconfig' ],
					outgoing: {},
				},
				'Portalconfig.searchBar': {
					chapterKey: 'Portalconfig.searchBar',
					h: 3,
					name: 'searchBar',
					label: 'searchbar',
					path: [ 'Portalconfig' ],
					outgoing: {},
				},
				Themenconfig: {
					chapterKey: 'Themenconfig',
					h: 2,
					label: 'bar',
					name: 'Themenconfig',
					path: [],
					outgoing: {},
				},
			},
			chapterOrder: [ 'Portalconfig', 'Themenconfig' ],
		},
		configTarget: vi.fn(() => true),
	})),
}));

import { useRoute } from 'vue-router';
vi.mock('vue-router', () => ({
	useRoute: vi.fn(() => ({
		name: 'EditConfig',
	})),
}));

import { useConfigStore } from '@/store/config';
vi.mock('@/store/config', () => ({
	useConfigStore: vi.fn(() => ({
		admintoolConfig: {},
	})),
}));

describe('chapters', () => {
	beforeEach(ctx => {
		setActivePinia(createPinia());
		ctx.store = useChaptersStore();
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});

	describe('activeStep', () => {
		it('provides the currently active step', ({ store }) => {
			const currentStep = store.activeStep;
			// introduction and layerconf are the first two steps
			expect(currentStep).toEqual(3);
		});
	});

	describe('chapters', () => {
		describe('without graph', () => {
			it('provides only introduction and loadconfig', ({ store }) => {
				useGraphStore.mockReturnValue({});
				const { chapters } = store;
				expect(chapters).toHaveLength(2);
				expect(chapters[0][0]).toEqual(expect.objectContaining({
					chapterKey: 'introduction',
				}));
				expect(chapters[1][0]).toEqual(expect.objectContaining({
					chapterKey: 'loadconfig',
				}));
			});
		});
		describe('with graph', () => {
			it('provides introduction and loadconfig', ({ store }) => {
				useGraphStore.mockReturnValue({});
				const { chapters } = store;
				expect(chapters[0][0]).toEqual(expect.objectContaining({
					chapterKey: 'introduction',
				}));
				expect(chapters[1][0]).toEqual(expect.objectContaining({
					chapterKey: 'loadconfig',
				}));
			});
			it('provides chapters from the graph', ({ store }) => {
				const { chapters } = store;
				expect(chapters[2][0]).toEqual(expect.objectContaining({
					chapterKey: 'Portalconfig',
				}));
				expect(chapters[2][1][0]).toEqual(expect.objectContaining({
					chapterKey: 'Portalconfig.menu',
				}));
				expect(chapters[2][1][1]).toEqual(expect.objectContaining({
					chapterKey: 'Portalconfig.searchBar',
				}));
				expect(chapters[3][0]).toEqual(expect.objectContaining({
					chapterKey: 'Themenconfig',
				}));
			});
			it('provides the export chapter as last chapter', ({ store }) => {
				const { chapters } = store;
				expect(chapters[4][0]).toEqual(expect.objectContaining({
					chapterKey: 'export',
				}));
			});
		});
		describe('excluded chapters', () => {
			it('takes excluded chapters into account', ({ store }) => {
				const chapterToExclude = 'portalconfig';
				useConfigStore.mockReturnValue({
					admintoolConfig: {
						excludedChapters: [ chapterToExclude ],
					},
				});
				const { chapters } = store;
				function chapterFinder (chapter) {
					return chapter.chapterKey.toLowerCase() === chapterToExclude.toLowerCase();
				}
				const excludedChapterIdx = chapters.indexOf(chapterFinder);
				expect(excludedChapterIdx).toBe(-1);
				expect(chapters).toHaveLength(4);
			});
			it('takes excluded subchapters into account', ({ store }) => {
				const chapterToExclude = 'portalconfig.menu';
				useConfigStore.mockReturnValue({
					admintoolConfig: {
						excludedChapters: [ chapterToExclude ],
					},
				});
				const { chapters } = store;
				function chapterFinder (chapter) {
					return chapter.chapterKey.toLowerCase() === chapterToExclude.toLowerCase();
				}
				// portalconfig is chapter[2]. subChapters are at chapter[2][1]
				const excludedSubChapterIdx = chapters[2][1].indexOf(chapterFinder);
				expect(excludedSubChapterIdx).toBe(-1);
				expect(chapters[2][1]).toHaveLength(1);
			});
		});
	});

	describe('getNextStep', () => {
		describe('forward', () => {
			it('gets the next subchapter', ({ store }) => {
				const currentRoute = {
					name: 'EditConfig',
					params: {
						path: 'Portalconfig.menu',
					},
				};
				const nextStep = store.getNextStep(currentRoute, true);
				expect(nextStep).toEqual(expect.objectContaining({
					chapterKey: 'Portalconfig.searchBar',
				}));
			});
			it('gets the next main chapter', ({ store }) => {
				const currentRoute = {
					name: 'EditConfig',
					params: {
						path: 'Portalconfig.searchBar',
					},
				};
				const nextStep = store.getNextStep(currentRoute, true);
				expect(nextStep).toEqual(expect.objectContaining({
					chapterKey: 'Themenconfig',
				}));
			});
		});
		describe('backwards', () => {
			it('gets previous subchapter, if current is subchapter', ({ store }) => {
				const currentRoute = {
					name: 'EditConfig',
					params: {
						path: 'Portalconfig.searchBar',
					},
				};
				const nextStep = store.getNextStep(currentRoute, false);
				expect(nextStep).toEqual(expect.objectContaining({
					chapterKey: 'Portalconfig.menu',
				}));
			});
			it('gets the current main chapter, if current is first subchapter', ({ store }) => {
				const currentRoute = {
					name: 'EditConfig',
					params: {
						path: 'Portalconfig.menu',
					},
				};
				const nextStep = store.getNextStep(currentRoute, false);
				expect(nextStep).toEqual(expect.objectContaining({
					chapterKey: 'Portalconfig',
				}));
			});
			it('gets the last subchapter of previous main chapter, if current is a main chapter', ({ store }) => {
				useRoute.mockReturnValue({
					name: 'SelectLayers',
				});
				const currentRoute = {
					name: 'SelectLayers',
				};
				const nextStep = store.getNextStep(currentRoute, false);
				expect(nextStep).toEqual(expect.objectContaining({
					chapterKey: 'Portalconfig.searchBar',
				}));
			});
			it('gets the previous main chapter, if current is main chapter and previous has no subchapter', ({ store }) => {
				const currentRoute = {
					name: 'EditConfig',
				};
				const nextStep = store.getNextStep(currentRoute, false);
				expect(nextStep).toEqual(expect.objectContaining({
					chapterKey: 'loadconfig',
				}));
			});
		});
	});
});
