import { describe, beforeEach, afterEach, it, expect, vi } from 'vitest';
import { setActivePinia, createPinia } from 'pinia';
import { useAlertStore } from '@/store/alert';

describe('alert', () => {
	beforeEach(ctx => {
		setActivePinia(createPinia());
		vi.useFakeTimers();
		ctx.store = useAlertStore();
	});
	afterEach(() => {
		vi.useRealTimers();
		vi.restoreAllMocks();
	});
	it('shows an alert and hides it again after time', ({ store }) => {
		store.error({
			content: 'TEST',
			options: 'OPTIONS',
		});
		expect(store.value).toBe(true);
		expect(store.type).toBe('error');
		expect(store.content).toBe('TEST');
		expect(store.options).toBe('OPTIONS');
		vi.runAllTimers();
		expect(store.value).toBe(false);
	});
	it('recognized the timeout option', ({ store }) => {
		store.info({
			content: 'TEST',
			timeout: 100000,
		});
		expect(store.value).toBe(true);
		vi.advanceTimersByTime(10000);
		expect(store.value).toBe(true);
		vi.runAllTimers();
		expect(store.value).toBe(false);
	});
});
