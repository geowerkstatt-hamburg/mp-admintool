import { ref, computed } from 'vue';
import { defineStore } from 'pinia';
import { useGraphStore } from '@/store/graph';
import { useLayersStore } from '@/store/layers';

function flattenTreeItem ({ Layer, Ordner }) {
	return [
		...(Layer ?? []),
		...(Ordner?.flatMap(flattenTreeItem) ?? []),
	];
}
function getFolder (themenconfig, category, path) {
	return path.reduce((pos, i) => pos.Ordner[i], themenconfig[category]);
}
function compareIds (a, b) {
	if (Array.isArray(a) !== Array.isArray(b)) {
		return false;
	}
	if (Array.isArray(a)) {
		return a.length === b.length && a.every((value, idx) => value === b[idx]);
	}
	return a === b;
}
function findById (pattern, { Ordner, Layer }) {
	return [
		Layer?.find(({ id }) => compareIds(id, pattern)),
		...(Ordner?.flatMap(pos => findById(pattern, pos)) ?? []),
	].find(it => it);
}
function deleteLayerById (pattern, { Ordner, Layer }) {
	Layer
		?.map(({ id }, idx) => [ id, idx ])
		.filter(([ id ]) => compareIds(id, pattern))
		.forEach(([ , idx ]) => {
			Layer.splice(idx, 1);
		});
	Ordner
		?.forEach(pos => deleteLayerById(pattern, pos));
}

export const useThemenconfigStore = defineStore('themenconfig', () => {
	const graph = useGraphStore();
	const layersStore = useLayersStore();

	const layers = ref([]);
	layersStore.onLoadLayers(() => {
		layersStore.getLightLayers().then(lightLayers => {
			layers.value = lightLayers;
		});
	});
	const layerMap = computed(() => new Map(layers.value.map(layer => ([ layer.id, layer ]))));

	const themenconfig = computed({
		get: () => graph.config.Themenconfig,
		set: value => {
			graph.config.Themenconfig = value;
		},
	});

	function getNamedLayers ({ Ordner, Layer, ...extra }) {
		return {
			...extra,
			...(Ordner ? {
				Ordner: Ordner.map(getNamedLayers),
			} : {}),
			Layer: Layer.map(layer => layer.name ? layer : {
				...layer,
				name: layerMap.value.get(layer.id)?.name,
			}),
		};
	}
	const namedThemenconfig = computed(() => ({
		Hintergrundkarten: getNamedLayers(themenconfig.value.Hintergrundkarten),
		Fachdaten: getNamedLayers(themenconfig.value.Fachdaten),
	}));

	function getTheFolder ({ category, path }) {
		return getFolder(themenconfig.value, category, path);
	}

	function addEntry ({ category, path, type, entry, idx }) {
		const folder = getFolder(themenconfig.value, category, path);
		if (!folder[type]) {
			folder[type] = [ entry ];
		} else if (typeof idx === 'number') {
			folder[type].splice(idx, 0, entry);
		} else {
			folder[type].push(entry);
		}
	}
	function updateEntry ({ category, path, type, idx, name, value }) {
		const folder = getFolder(themenconfig.value, category, path);
		folder[type][idx][name] = value;
	}
	function moveEntry ({ category, path, type, idx, pos }) {
		const folder = getFolder(themenconfig.value, category, path);
		const entries = folder[type];
		const [ entry ] = entries.splice(idx, 1);
		entries.splice(pos, 0, entry);
	}
	function deleteEntry ({ category, path, type, idx }) {
		const folder = getFolder(themenconfig.value, category, path);
		return folder[type].splice(idx, 1)[0];
	}

	function updateById ({ id, name, value }) {
		const layer = Object.values(themenconfig.value).map(rootFolder => findById(id, rootFolder)).find(it => it);
		layer[name] = value;
	}
	function deleteById ({ id }) {
		Object.values(themenconfig.value).forEach(pos => deleteLayerById(id, pos));
	}

	const selectedLayers = computed(() => ([
		themenconfig.value?.Fachdaten,
		themenconfig.value?.Hintergrundkarten,
	].map(it => it ?? {}).flatMap(flattenTreeItem)));
	function flattenThemenconfig () {
		themenconfig.value = {
			Hintergrundkarten: {
				Layer: [],
			},
			Fachdaten: {
				Layer: selectedLayers.value,
			},
		};
	}
	function unflattenThemenconfig () {
		themenconfig.value = {
			Hintergrundkarten: {
				Ordner: themenconfig.value?.Hintergrundkarten?.Ordner ?? [],
				Layer: themenconfig.value?.Hintergrundkarten?.Layer ?? [],
			},
			Fachdaten: {
				Ordner: themenconfig.value?.Fachdaten?.Ordner ?? [],
				Layer: themenconfig.value?.Fachdaten?.Layer ?? [],
			},
		};
	}
	const treeType = computed({
		get: () => graph.config.Portalconfig?.treeType,
		set: value => {
			switch (value) {
				case 'light':
					flattenThemenconfig();
					break;
				case 'custom':
				default:
					unflattenThemenconfig();
					break;
			}
			graph.config.Portalconfig.treeType = value;
		},
	});

	function clear () {
		switch (treeType.value) {
			case 'light':
				themenconfig.value = {
					Hintergrundkarten: {
						Layer: [],
					},
					Fachdaten: {
						Layer: [],
					},
				};
				break;
			case 'custom':
			default:
				themenconfig.value = {
					Hintergrundkarten: {
						Ordner: [],
						Layer: [],
					},
					Fachdaten: {
						Ordner: [],
						Layer: [],
					},
				};
				break;
		}
	}

	return {
		themenconfig: computed(() => namedThemenconfig.value),
		selectedLayers,
		getFolder: getTheFolder,
		add: addEntry,
		update: updateEntry,
		move: moveEntry,
		delete: deleteEntry,
		updateById,
		deleteById,
		treeType,
		clear,
	};
});
