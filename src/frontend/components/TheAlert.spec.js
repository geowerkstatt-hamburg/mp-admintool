import { describe, it, expect, afterEach, vi } from 'vitest';
import { render } from '@/test/components/helper';

import TheAlert from '@/components/TheAlert.vue';
import { useAlertStore } from '@/store/alert';

describe('TheAlert', () => {
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', () => {
		useAlertStore.mockReturnValue({
			value: true,
			type: 'error',
			content: 'CONTENT',
			options: { OPTIONS: 'yes' },
		});
		const { getByText, baseElement } = render(TheAlert);
		getByText('{$t(CONTENT, {"OPTIONS":"yes"})}');
		expect(baseElement).toMatchSnapshot();
	});
});
