import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { fireEvent } from '@testing-library/vue';
import { flushPromises } from '@vue/test-utils';
import { render } from '@/test/components/helper';
import { ref, nextTick } from 'vue';

import QuickPreview from '@/components/Export/QuickPreview.vue';

import { disablePreview } from '@/lib/export';

vi.mock('@/lib/export', () => ({
	uploadConfig: vi.fn(() => Promise.resolve('URL')),
	getMasterportalPreviewURL: vi.fn(path => `MPP/${path}`),
	disablePreview: ref(false),
}));

describe('QuickPreview', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		disablePreview.value = false;
		ctx.render = render(QuickPreview, {
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('matches snapshot when preview disabled', async ({ render: { container } }) => {
		disablePreview.value = true;
		await nextTick();
		expect(container).toMatchSnapshot();
	});
	it('opens the right URL', async ({ render: { getByText } }) => {
		await fireEvent.click(getByText('{$t(chapters.export.quickPreview.title)}'));
		await flushPromises();
		expect(window.open).toHaveBeenCalledWith('MPP/URL', '_blank');
	});
});
