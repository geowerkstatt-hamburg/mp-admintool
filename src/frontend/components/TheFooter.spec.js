import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { render } from '@/test/components/helper';

import TheFooter from '@/components/TheFooter.vue';
import { usePageLayoutStyleStore } from '@/store/pageLayoutStyle';

describe('TheFooter', () => {
	beforeEach(() => {
		document.body.innerHTML = '';
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', () => {
		usePageLayoutStyleStore.mockReturnValue({
			footerStyle: {
				color: 'red',
			},
		});
		const { container } = render(TheFooter);
		expect(container).toMatchSnapshot();
	});
});
