import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { reactive, nextTick } from 'vue';
import { render } from '@/test/components/helper';

import TheLoadingIndicator from '@/components/TheLoadingIndicator.vue';
import { useLoadingStore } from '@/store/loading';

describe('TheLoadingIndicator', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		useLoadingStore.mockReturnValue(reactive({
			loading: false,
		}));
		ctx.render = render(TheLoadingIndicator);
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', async ({ render: { container } }) => {
		useLoadingStore().loading = true;
		await nextTick();
		expect(container).toMatchSnapshot('loading indeterminate');

		useLoadingStore().loading = false;
		await nextTick();
		expect(container).toMatchSnapshot('inactive');
	});
});
