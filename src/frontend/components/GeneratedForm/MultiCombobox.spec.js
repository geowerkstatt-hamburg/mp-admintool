import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { fireEvent } from '@testing-library/vue';
import { render } from '@/test/components/helper';

import MultiCombobox from '@/components/GeneratedForm/MultiCombobox.vue';

describe('MultiCombobox', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		ctx.render = render(MultiCombobox, {
			props: {
				edge: {
					label: 'LABEL',
					description: 'DESC',
					default: [ 'A' ],
					required: true,
					deprecated: false,
				},
				modelValue: [ 1, 2, 3, 4, 'invalid' ].map((content, id) => ({ content, id })),
				rules: [ v => v.every(o => o.title !== 'invalid') || 'invalid' ],
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('renders the label and description', ({ render: { getByText, getAllByText } }) => {
		getAllByText('LABEL');
		getByText('DESC');
	});
	it('allows moving elements', ({ render: { getAllByText, emitted } }) => {
		fireEvent.click(getAllByText('chevron_right')[0]);
		expect(emitted()['update:modelValue']).toBeTruthy();
		expect(emitted()['update:modelValue'].length).toBe(1);
		expect(emitted()['update:modelValue'][0]?.[0].map(({ content }) => content)).toEqual([ 2, 1, 3, 4, 'invalid' ]);
	});

	function hasClass (node, className) {
		return node.classList.contains(className)
			|| node.parentElement && hasClass(node.parentElement, className);
	}
	it('renders invalid entries red', ({ render: { getByText } }) => {
		expect(hasClass(getByText('1'), 'text-red')).toBeFalsy();
		expect(hasClass(getByText('2'), 'text-red')).toBeFalsy();
		expect(hasClass(getByText('3'), 'text-red')).toBeFalsy();
		expect(hasClass(getByText('4'), 'text-red')).toBeFalsy();
		expect(hasClass(getByText('invalid'), 'text-red')).toBeTruthy();
	});
});
