import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { fireEvent } from '@testing-library/vue';
import { render } from '@/test/components/helper';

import CollapseBtn from '@/components/GeneratedForm/collapse/CollapseBtn.vue';

describe('CollapseBtn', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		ctx.render = render(CollapseBtn, {
			props: {
				modelValue: false,
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('toggles value on click', ({ render: { getByRole, emitted } }) => {
		fireEvent.click(getByRole('button'));
		expect(emitted()['update:modelValue']).toBeTruthy();
		expect(emitted()['update:modelValue'].length).toBe(1);
		expect(emitted()['update:modelValue'][0]).toEqual([ true ]);
	});
});
