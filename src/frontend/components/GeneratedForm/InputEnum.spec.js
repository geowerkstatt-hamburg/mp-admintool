import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { fireEvent } from '@testing-library/vue';
import { render } from '@/test/components/helper';

import InputEnum from '@/components/GeneratedForm/InputEnum.vue';
import { BasicFieldTypes } from '@masterportal/mpconfigparser';

describe('InputEnum', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		ctx.render = render(InputEnum, {
			props: {
				node: {},
				edge: {
					label: 'LABEL',
					types: [ [ BasicFieldTypes.enum, [ 'A', 'B' ] ] ],
					required: true,
				},
				typeIndex: 0,
				configPath: [],
				/* eslint-disable-next-line no-undefined */
				modelValue: undefined,
			},
			stubs: {
				'tooltip-icon': true,
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('sets only valid values and falls back to falsy string otherwise', ({ render: { getByLabelText, emitted } }) => {
		const field = getByLabelText('LABEL');
		fireEvent.update(field, 'A');
		fireEvent.update(field, 'B');
		fireEvent.update(field, 'invalid');

		expect(emitted()['update:modelValue']).toBeTruthy();
		expect(emitted()['update:modelValue'].length).toBe(3);
		expect(emitted()['update:modelValue'][0]).toEqual([ 'A' ]);
		expect(emitted()['update:modelValue'][1]).toEqual([ 'B' ]);
		expect(emitted()['update:modelValue'][2]).toEqual([ '' ]);
	});
	it('features a multi mode', async ({ render: { rerender, getByText, getByLabelText, emitted } }) => {
		await rerender({
			node: {},
			edge: {
				label: 'LABEL',
				types: [ [ Array, [ BasicFieldTypes.enum, [ 'A', 'B' ] ] ] ],
				required: true,
			},
			typeIndex: 0,
			configPath: [],
			/* eslint-disable-next-line no-undefined */
			modelValue: undefined,
			multi: true,
		});

		fireEvent.update(getByLabelText('LABEL'), null);
		fireEvent.update(getByText('A'));
		fireEvent.update(getByText('B'));

		expect(emitted()['update:modelValue']).toBeTruthy();
		expect(emitted()['update:modelValue'].length).toBe(3);
		expect(emitted()['update:modelValue'][0]).toEqual([ [] ]);
		expect(emitted()['update:modelValue'][1]).toEqual([ [ 'A' ] ]);
		expect(emitted()['update:modelValue'][2]).toEqual([ [ 'A', 'B' ] ]);
	});
});
