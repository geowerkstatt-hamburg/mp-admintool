import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { fireEvent } from '@testing-library/vue';
import { render } from '@/test/components/helper';

import MapElement from '@/components/GeneratedForm/mapPreview/MapElement.vue';

let mapInputParams;
const mapInputGetCoord = vi.fn();
vi.mock('@/lib/MapInput', () => ({
	default: class {
		constructor (map, type, coord, setter) {
			mapInputParams = { map, type, coord, setter };
		}
		getCoordinates () {
			return mapInputGetCoord();
		}
		reRender () { /**/ }
	},
}));

vi.mock('@/lib/showMap', () => ({
	default: vi.fn(() => Promise.resolve(Symbol.for('Map'))),
}));

describe('MapElement', () => {
	beforeEach(ctx => {
		mapInputParams = null;
		document.body.innerHTML = '';
		ctx.render = render(MapElement, {
			props: {
				targetName: 'map',
				type: 'point',
				epsgCode: 'EPSG:25832',
				modelValue: [ 561210, 5932600 ],
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('passes coordinates correctly', ({ render: { emitted } }) => {
		expect(mapInputParams).toBeTruthy();
		expect(mapInputParams).toHaveProperty('map', Symbol.for('Map'));
		expect(mapInputParams).toHaveProperty('type', 'point');
		expect(mapInputParams).toHaveProperty('coord', [ 561210, 5932600 ]);
		expect(mapInputParams).toHaveProperty('setter');
		mapInputGetCoord.mockReturnValue([ 576666, 6028205 ]);
		mapInputParams.setter();
		expect(emitted()['update:modelValue']).toBeTruthy();
		expect(emitted()['update:modelValue'].length).toBe(1);
		expect(emitted()['update:modelValue'][0]).toEqual([ [ 576666, 6028205 ] ]);
	});
	it('expands on click', async ({ render: { getByRole, getByTestId } }) => {
		expect(getByTestId('map').classList.contains('mapExpanded')).toBe(false);
		await fireEvent.click(getByRole('button'));
		expect(getByTestId('map').classList.contains('mapExpanded')).toBe(true);
		await fireEvent.click(getByRole('button'));
		expect(getByTestId('map').classList.contains('mapExpanded')).toBe(false);
	});
});
