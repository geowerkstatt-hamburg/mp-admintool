import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { render } from '@/test/components/helper';

import TooltipIcon from '@/components/GeneratedForm/tooltips/TooltipIcon.vue';

describe('TooltipIcon', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		ctx.render = render(TooltipIcon, {
			props: {
				description: 'DESC',
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
});
