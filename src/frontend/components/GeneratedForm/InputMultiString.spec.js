import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { render } from '@/test/components/helper';

import InputMultiString from '@/components/GeneratedForm/InputMultiString.vue';
import { BasicFieldTypes } from '@masterportal/mpconfigparser';

describe('InputMultiString', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		ctx.render = render(InputMultiString, {
			props: {
				node: {},
				edge: {
					label: 'LABEL',
					types: [ [ Array, BasicFieldTypes.string ] ],
					default: '',
					required: true,
				},
				typeIndex: 0,
				configPath: [],
				modelValue: [ 'A', 'B', 'C' ],
			},
			stubs: {
				'MultiCombobox': {
					props: [ 'modelValue', 'edge' ],
					template: '§ MultiCombobox({{ JSON.stringify(modelValue, null, "\t") }}/{{ JSON.stringify(edge, null, "\t") }})',
				},
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches inline snapshot', ({ render: { getByText } }) => {
		expect(getByText(/^§ MultiCombobox\(.+\)$/).innerHTML).toMatchInlineSnapshot(`
			§ MultiCombobox([
				{
					"content": "A",
					"id": 0
				},
				{
					"content": "B",
					"id": 1
				},
				{
					"content": "C",
					"id": 2
				}
			]/{
				"label": "LABEL",
				"types": [
					[
						null,
						null
					]
				],
				"default": "",
				"required": true
			})
		`);
	});
});
