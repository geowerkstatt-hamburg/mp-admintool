import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { fireEvent } from '@testing-library/vue';
import { render } from '@/test/components/helper';

import InputString from '@/components/GeneratedForm/InputString.vue';
import { BasicFieldTypes } from '@masterportal/mpconfigparser';

describe('InputString', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		ctx.render = render(InputString, {
			props: {
				node: {},
				edge: {
					label: 'LABEL',
					types: [ BasicFieldTypes.string ],
					default: '',
					required: true,
				},
				typeIndex: 0,
				configPath: [],
				/* eslint-disable-next-line no-undefined */
				modelValue: undefined,
			},
			stubs: {
				'tooltip-icon': true,
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('emits strings on update', ({ render: { getByLabelText, emitted } }) => {
		const field = getByLabelText('LABEL');
		fireEvent.update(field, '42');
		fireEvent.update(field, '123.456ABC');
		fireEvent.update(field, 'AS123');

		expect(emitted()['update:modelValue']).toBeTruthy();
		expect(emitted()['update:modelValue'].length).toBe(3);
		expect(emitted()['update:modelValue'][0]).toEqual([ '42' ]);
		expect(emitted()['update:modelValue'][1]).toEqual([ '123.456ABC' ]);
		expect(emitted()['update:modelValue'][2]).toEqual([ 'AS123' ]);
	});
});
