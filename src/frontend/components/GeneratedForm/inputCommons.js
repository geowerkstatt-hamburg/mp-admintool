/**
 * All input elements share at least these input props. Put here to avoid duplication.
 * @typedef {object} inputProps
 * @property {object} node node of graph that is worked on
 * @property {object} edge edge of node that is worked on
 * @property {object} typeIndex if edge is multi-type, x-th type is to be used
 * @property {object} configPath path to relevant spot in config object
 * @property {object} getValue getter function for model
 * @property {object} setValue setter function for model
 * @property {object} multi whether element should produce an array of type
 */
export const inputProps = {
	node: {
		type: Object,
		required: true,
	},
	edge: {
		type: Object,
		required: true,
	},
	typeIndex: {
		type: Number,
		required: true,
	},
	configPath: {
		type: Array,
		required: true,
	},
	modelValue: {
		required: true,
	},
	tableConfig: {
		type: Object,
		default: null,
	},
};

export const inputEmits = [ 'update:modelValue' ];
