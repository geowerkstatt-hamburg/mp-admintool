import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { fireEvent } from '@testing-library/vue';
import { render } from '@/test/components/helper';

import InputNumber from '@/components/GeneratedForm/InputNumber.vue';
import { BasicFieldTypes } from '@masterportal/mpconfigparser';

describe('InputNumber', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		ctx.render = render(InputNumber, {
			props: {
				node: {},
				edge: {
					label: 'LABEL',
					types: [ BasicFieldTypes.float ],
					default: 500,
					required: true,
				},
				typeIndex: 0,
				configPath: [],
				/* eslint-disable-next-line no-undefined */
				modelValue: undefined,
			},
			stubs: {
				'tooltip-icon': true,
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('sets numbers (only) and falls back to zero', ({ render: { getByLabelText, emitted } }) => {
		fireEvent.update(getByLabelText('LABEL'), '42');
		fireEvent.update(getByLabelText('LABEL'), '123.456ABC');
		fireEvent.update(getByLabelText('LABEL'), 'AS123');

		expect(emitted()['update:modelValue']).toBeTruthy();
		expect(emitted()['update:modelValue'].length).toBe(3);
		expect(emitted()['update:modelValue'][0]).toEqual([ 42 ]);
		expect(emitted()['update:modelValue'][1]).toEqual([ 123.456 ]);
		expect(emitted()['update:modelValue'][2]).toEqual([ 0 ]);
	});
});
