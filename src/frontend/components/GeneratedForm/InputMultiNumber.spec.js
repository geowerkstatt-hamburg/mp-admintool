import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { render } from '@/test/components/helper';

import InputMultiNumber from '@/components/GeneratedForm/InputMultiNumber.vue';
import { BasicFieldTypes } from '@masterportal/mpconfigparser';

describe('InputMultiNumber', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		ctx.render = render(InputMultiNumber, {
			props: {
				node: {},
				edge: {
					label: 'LABEL',
					types: [ [ Array, BasicFieldTypes.integer ] ],
					default: '',
					required: true,
				},
				typeIndex: 0,
				configPath: [],
				modelValue: [ '23', '42.37', 10000000000000000000000000000000000000 ],
			},
			stubs: {
				'MultiCombobox': {
					props: [ 'modelValue', 'rules', 'edge' ],
					emits: [ 'update:modelValue' ],
					setup (props, setupCtx) {
						ctx.emit = setupCtx.emit;
					},
					template: `§ MultiCombobox({{
						JSON.stringify(modelValue, null, '\t')
					}}/{{
						JSON.stringify(edge, null, '\t')
					}}/{{
						rules.length
					}}!{{
						rules.reduce((err, it) => err === true
							? it(modelValue.map(({ content, id }) => ({
								title: content,
								value: id,
							})))
							: err,
						true)
					}})`,
				},
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches inline snapshot', ({ render: { getByText } }) => {
		expect(getByText(/^§ MultiCombobox\(.+\)$/).innerHTML).toMatchInlineSnapshot(`
			§ MultiCombobox([
				{
					"content": "23",
					"id": 0
				},
				{
					"content": "42.37",
					"id": 1
				},
				{
					"content": "10000000000000000000000000000000000000",
					"id": 2
				}
			]/{
				"label": "LABEL",
				"types": [
					[
						null,
						[
							null,
							"Integer"
						]
					]
				],
				"default": "",
				"required": true
			}/1!{$t(forms.inputNumber.invalidInteger)})
		`);
	});
	it('distinguishes between integer and float type', async ({ render: { getByText, rerender } }) => {
		getByText(/\$t\(forms\.inputNumber\.invalidInteger\)/);
		expect(() => getByText(/\$t\(forms\.inputNumber\.invalidFloat\)/)).toThrowError();

		await rerender({
			node: {},
			edge: {
				label: 'LABEL',
				types: [ [ Array, BasicFieldTypes.float ] ],
				default: '',
				required: true,
			},
			typeIndex: 0,
			configPath: [],
			modelValue: [ '23', '42.37' ],
		});
		expect(() => getByText(/\$t\(forms\.inputNumber\.invalidInteger\)/)).toThrowError();
		expect(() => getByText(/\$t\(forms\.inputNumber\.invalidFloat\)/)).toThrowError();

		await rerender({
			node: {},
			edge: {
				label: 'LABEL',
				types: [ [ Array, BasicFieldTypes.float ] ],
				default: '',
				required: true,
			},
			typeIndex: 0,
			configPath: [],
			modelValue: [ '23', '42.37', 'invalid' ],
		});
		expect(() => getByText(/\$t\(forms\.inputNumber\.invalidInteger\)/)).toThrowError();
		getByText(/\$t\(forms\.inputNumber\.invalidFloat\)/);
	});
	it('emits numbers instead of strings on update', ctx => {
		ctx.emit('update:modelValue', [ '1', '12345678909876543212345678' ]);
		expect(ctx.render.emitted()['update:modelValue']).toBeTruthy();
		expect(ctx.render.emitted()['update:modelValue'].length).toBe(1);
		/* eslint-disable-next-line no-loss-of-precision */
		expect(ctx.render.emitted()['update:modelValue'][0]).toEqual([ [ 1, 12345678909876543212345678 ] ]);
	});
});
