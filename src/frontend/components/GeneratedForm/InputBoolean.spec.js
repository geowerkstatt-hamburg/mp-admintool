import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { fireEvent } from '@testing-library/vue';
import { render } from '@/test/components/helper';

import InputBoolean from '@/components/GeneratedForm/InputBoolean.vue';
import { BasicFieldTypes } from '@masterportal/mpconfigparser';

describe('InputBoolean', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		ctx.render = render(InputBoolean, {
			props: {
				node: {},
				edge: {
					label: 'LABEL',
					types: [ BasicFieldTypes.boolean ],
					default: false,
					required: true,
				},
				typeIndex: 0,
				configPath: [],
				/* eslint-disable-next-line no-undefined */
				modelValue: undefined,
			},
			stubs: {
				'tooltip-icon': true,
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('works correctly', async ({ render: { getByText, rerender, emitted } }) => {
		fireEvent.click(getByText('LABEL'));
		await rerender({ modelValue: true });
		fireEvent.click(getByText('LABEL'));
		await rerender({ modelValue: false });
		fireEvent.click(getByText('LABEL'));

		expect(emitted()['update:modelValue']).toBeTruthy();
		expect(emitted()['update:modelValue'].length).toBe(3);
		expect(emitted()['update:modelValue'][0]).toEqual([ true ]);
		expect(emitted()['update:modelValue'][1]).toEqual([ false ]);
		expect(emitted()['update:modelValue'][2]).toEqual([ true ]);
	});
	it('sets only boolean values', async ({ render: { getByText, rerender, emitted } }) => {
		await rerender({ modelValue: 'stringish-truthy value' });
		// should be true now, so toggling shall set to false
		fireEvent.click(getByText('LABEL'));

		await rerender({ modelValue: '' });
		// should be false now, so toggling shall set to true
		fireEvent.click(getByText('LABEL'));

		expect(emitted()['update:modelValue']).toBeTruthy();
		expect(emitted()['update:modelValue'].length).toBe(2);
		expect(emitted()['update:modelValue'][0]).toEqual([ false ]);
		expect(emitted()['update:modelValue'][1]).toEqual([ true ]);
	});
});
