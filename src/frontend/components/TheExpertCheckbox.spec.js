import { describe, it, expect, afterEach, vi } from 'vitest';
import { render } from '@/test/components/helper';

import TheExpertCheckbox from '@/components/TheExpertCheckbox.vue';
import { useRoute } from 'vue-router';
import { useExpertModeStore } from '@/store/expertMode';
import { useConfigStore } from '@/store/config';

describe('TheExpertCheckbox', () => {
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('is visible if it should be', () => {
		useExpertModeStore.mockReturnValue({
			expertMode: false,
		});
		useConfigStore.mockReturnValue({
			admintoolConfig: {
				expertMode: true,
			},
		});
		useRoute.mockReturnValue({
			path: '/editConfig/Portalconfig',
		});
		const { getByText, baseElement } = render(TheExpertCheckbox);
		getByText('{$t(controls.expertMode.label)}');
		expect(baseElement).toMatchSnapshot();
	});
	it('is invisible if it should be', () => {
		useExpertModeStore.mockReturnValue({
			expertMode: false,
		});
		useConfigStore.mockReturnValue({
			admintoolConfig: {
				expertMode: true,
			},
		});
		useRoute.mockReturnValue({
			path: '/editConfig/Portalconfig',
		});
		const { getByText } = render(TheExpertCheckbox);
		expect(() => getByText('{$t(controls.expertMode.label)}')).toThrowError();
	});
});
