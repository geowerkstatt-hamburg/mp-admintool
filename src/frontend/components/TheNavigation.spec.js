import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { render } from '@/test/components/helper';
import { fireEvent } from '@testing-library/vue';

import TheNavigation from '@/components/TheNavigation.vue';
import { useRouter } from 'vue-router';
import { useChaptersStore } from '@/store/chapters';

describe('TheNavigation', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		useChaptersStore.mockReturnValue({
			chapters: [
				[ { label: 'ARoute', route: { name: 'A' }, icon: 'edit', rules: [ () => false ] } ],
				[ { label: 'BRoute', route: { name: 'B' }, icon: 'edit', rules: [] } ],
				[ { label: 'CRoute', route: { name: 'C' }, icon: 'edit', rules: [] } ],
			],
		});
		ctx.router = useRouter();
		ctx.render = render(TheNavigation, {
			stubs: {
				'a-stepper': {
					props: {
						activeStep: Number,
						steps: Number,
					},
					template: '<slot />',
				},
				'a-stepper-step': {
					props: {
						step: Number,
						to: Object,
					},
					template: '<v-btn :to="to"><slot /></v-btn>',
				},
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('allows navigation to other chapters', async ({ render: { getByText }, router }) => {
		expect(router.push).not.toHaveBeenCalled();
		await fireEvent.click(getByText('{$t(ARoute)}'));
		expect(router.push).toHaveBeenCalled();
	});
});
