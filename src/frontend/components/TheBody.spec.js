import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { reactive, nextTick } from 'vue';
import { render } from '@/test/components/helper';

import TheBody from '@/components/TheBody.vue';
import { usePageLayoutStyleStore } from '@/store/pageLayoutStyle';

describe('TheBody', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		usePageLayoutStyleStore.mockReturnValue(reactive({
			isBodyHidden: false,
		}));
		ctx.render = render(TheBody, {
			slots: {
				default: 'TEST',
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('renders default slot', ({ render: { getByText } }) => {
		getByText('TEST');
	});
	it('renders default slot even if hidden', async ({ render: { getByText } }) => {
		usePageLayoutStyleStore().isBodyHidden = true;
		await nextTick();
		getByText('TEST');
	});
	it('hides body if store points to do so', async ({ render: { container } }) => {
		expect(window.getComputedStyle(container.firstChild).display).not.toBe('none');
		usePageLayoutStyleStore().isBodyHidden = true;
		await nextTick();
		expect(window.getComputedStyle(container.firstChild).display).toBe('none');
	});
});
