import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { fireEvent } from '@testing-library/vue';
import { render } from '@/test/components/helper';

import SelectLayerElement from '@/components/Themenconfig/SelectLayerElement.vue';
import { usePreviewStore } from '@/store/preview';

describe('SelectLayerElement', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		usePreviewStore.mockReturnValue({
			displayMap: ctx.previewFn = vi.fn(),
		});
		ctx.render = render(SelectLayerElement, {
			props: {
				item: {
					id: 100,
					name: 'testWMS100',
					type: 'WMS',
					selected: 0,
					visibility: 0,
				},
				type: 'unselectedLayers',
				index: 5,
				first: false,
				last: false,
			},
			stubs: {
				'tooltip-icon': true,
				'v-icon': {
					props: [ 'icon' ],
					template: 'ICON({{ icon }})',
				},
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('shows preview when clicking on map symbol', ({ render: { getByText }, previewFn }) => {
		fireEvent.click(getByText('ICON(map)'));
		expect(previewFn).toHaveBeenCalledWith({ layerId: 100 });
	});
	it('shows visibility toggle button for selectedLayers view and sends event', async ({ render: { getByText, rerender, emitted } }) => {
		expect(() => getByText(/ICON\(visibility.*\)/)).toThrowError();

		await rerender({
			item: {
				id: 101,
				name: 'testWMS101',
				type: 'WMS',
				selected: 1,
				visibility: 0,
			},
			type: 'selectedLayers',
		});
		fireEvent.click(getByText('ICON(visibility_off)'));
		expect(emitted().toggleVisibility).toBeTruthy();
		expect(emitted().toggleVisibility.length).toBe(1);
		expect(emitted().toggleVisibility[0]).toEqual([]);
	});
});
