import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { render } from '@/test/components/helper';
import { fireEvent } from '@testing-library/vue';

import TheLocaleSwitch from '@/components/TheLocaleSwitch.vue';
import { useTranslation } from 'i18next-vue';

describe('TheLocaleSwitch', () => {
	beforeEach(() => {
		document.body.innerHTML = '';
		useTranslation.mockReturnValue({
			i18next: {
				language: 'de',
				changeLanguage: vi.fn(),
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', () => {
		const { container } = render(TheLocaleSwitch);
		expect(container).toMatchSnapshot();
	});

	it('inititializes correctly and locale switcher exists', () => {
		const { getByText, getByDisplayValue } = render(TheLocaleSwitch);
		getByDisplayValue('Deutsch');
		getByText('English');
	});

	it('changes language', () => {
		const { getByDisplayValue } = render(TheLocaleSwitch);
		const select = getByDisplayValue('Deutsch');
		fireEvent.update(select, 'en');
		expect(useTranslation().i18next.changeLanguage).toBeCalledWith('en');
	});
});
