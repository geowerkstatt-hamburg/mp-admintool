import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { render } from '@/test/components/helper';

import PageLayout from '@/components/PageLayout.vue';
import { ref } from 'vue';
import { useChaptersStore } from '@/store/chapters';

describe('PageLayout', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		useChaptersStore.mockReturnValue({
			chapters: [
				[ { label: 'ARoute', route: { name: 'A' }, icon: 'edit', rules: [ () => false ] } ],
				[ { label: 'BRoute', route: { name: 'B' }, icon: 'edit', rules: [] } ],
				[ { label: 'CRoute', route: { name: 'C' }, icon: 'edit', rules: [] } ],
			],
		});
		ctx.title = ref('TEST');
		ctx.render = render(PageLayout, {
			provide: {
				title: ctx.title,
			},
			props: {
				showNavButtons: true,
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
});
