import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { reactive, nextTick } from 'vue';
import { render } from '@/test/components/helper';

import TheMap from '@/components/TheMap.vue';

import { usePreviewStore } from '@/store/preview';
import { usePageLayoutStyleStore } from '@/store/pageLayoutStyle';

import showMap from '@/lib/showMap';
vi.mock('@/lib/showMap', () => ({
	default: vi.fn(),
}));

const setStyleName = vi.fn();

describe('TheMap', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		usePreviewStore.mockReturnValue(reactive({
			enabled: false,
			layerId: null,
			epsgCode: 'EPSG:25832',
		}));
		usePageLayoutStyleStore.mockReturnValue({
			setStyleName,
		});
		showMap.mockImplementation((layerId, styleId, epsgCode) => {
			ctx.render.baseElement.querySelector('#map').innerHTML = `TESTMAP(${layerId}/${epsgCode})`;
		});
		ctx.render = render(TheMap);
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', async ({ render: { container } }) => {
		usePreviewStore().layerId = 'TEST';
		usePreviewStore().enabled = true;
		await nextTick();
		expect(container).toMatchSnapshot();
	});
	it('should detect and handle enabling and disabling preview from store', async ({ render: { getByText, getAllByText } }) => {
		usePreviewStore().layerId = 'TEST';
		usePreviewStore().enabled = true;
		await nextTick();
		expect(setStyleName).toHaveBeenLastCalledWith('overlay');
		expect(showMap).toHaveBeenCalled();
		getByText('TESTMAP(TEST/EPSG:25832)');

		usePreviewStore().enabled = false;
		await nextTick();
		expect(setStyleName).toHaveBeenLastCalledWith('default');
		expect(() => getAllByText('TESTMAP(TEST/EPSG:25832)')).toThrowError();
	});
});
