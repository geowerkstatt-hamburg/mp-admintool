import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { render } from '@/test/components/helper';

import TheStorageDialog from '@/components/TheStorageDialog.vue';
import { useTranslation } from 'i18next-vue';
import { useGraphStore } from '@/store/graph';

describe('TheStorageDialog', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		useTranslation.mockReturnValue({
			i18next: {
				language: 'de',
			},
		});
		useGraphStore.mockReturnValue({
			version: 'v1.2.3-test',
			_lastModified: Date.parse('17 Aug 2018 07:05:43'),
		});
		ctx.render = render(TheStorageDialog);
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('contains timestamp and version', ({ render: { getByText } }) => {
		getByText(/v1\.2\.3-test/);
		getByText(/17\. August 2018/);
	});
});
