import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { render } from '@/test/components/helper';

import TheHeader from '@/components/TheHeader.vue';
import { usePageLayoutStyleStore } from '@/store/pageLayoutStyle';

describe('TheHeader', () => {
	beforeEach(() => {
		document.body.innerHTML = '';
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', () => {
		usePageLayoutStyleStore.mockReturnValue({
			footerStyle: {
				color: 'red',
			},
		});
		const { container } = render(TheHeader);
		expect(container).toMatchSnapshot();
	});
});
