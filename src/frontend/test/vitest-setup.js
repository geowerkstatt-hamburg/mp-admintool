/* eslint-disable no-undef */ // for globalThis

import 'fake-indexeddb/auto';

import { expect } from 'vitest';

import vueSnapshotSerializer from './vitest-serializer-vue';
expect.addSnapshotSerializer(vueSnapshotSerializer);

import testSnapshotSerializer from './vitest-serializer-test';
expect.addSnapshotSerializer(testSnapshotSerializer);
