export default {
	test: received => typeof received === 'string' && received.charAt(0) === '§',
	print: received => received,
};
