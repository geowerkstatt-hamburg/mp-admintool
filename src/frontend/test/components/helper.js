import { vi } from 'vitest';
import { render as libRender } from '@testing-library/vue';
import { reactive } from 'vue';
import { createTestingPinia } from '@pinia/testing';
import vuetifyPlugin from '@/plugins/vuetify';
import { upperFirst } from '@/lib/stringHelper';

import VDialogStub from './VDialogStub.vue';
import VSelectStub from './VSelectStub.vue';
import VBtnStub from './VBtnStub.vue';

import { useRoute, useRouter } from 'vue-router';
const route = reactive({
	meta: {},
});
const routerPush = vi.fn();
vi.mock('vue-router', () => ({
	useRoute: vi.fn(() => route),
	useRouter: () => ({
		push: routerPush,
	}),
}));

import { useTranslation } from 'i18next-vue';
const translateFn = vi.fn((it, opts) => opts
	? `{$t(${it}, ${JSON.stringify(opts)})}`
	: `{$t(${it})}`);
vi.mock('i18next-vue', () => ({
	useTranslation: vi.fn(() => ({
		i18next: {
			t: translateFn,
		},
	})),
}));

import { useAlertStore } from '@/store/alert';
vi.mock('@/store/alert', () => ({
	useAlertStore: vi.fn(),
}));

import { useChaptersStore } from '@/store/chapters';
vi.mock('@/store/chapters', () => ({
	useChaptersStore: vi.fn(),
}));

import { useConfigStore } from '@/store/config';
vi.mock('@/store/config', () => ({
	useConfigStore: vi.fn(),
}));

import { useExpertModeStore } from '@/store/expertMode';
vi.mock('@/store/expertMode', () => ({
	useExpertModeStore: vi.fn(),
}));

import { useGraphStore } from '@/store/graph';
vi.mock('@/store/graph', () => ({
	useGraphStore: vi.fn(),
}));

import { useLayersStore } from '@/store/layers';
vi.mock('@/store/layers', () => ({
	useLayersStore: vi.fn(),
}));

import { useLoadingStore } from '@/store/loading';
vi.mock('@/store/loading', () => ({
	useLoadingStore: vi.fn(),
}));

import { usePageLayoutStyleStore } from '@/store/pageLayoutStyle';
vi.mock('@/store/pageLayoutStyle', () => ({
	usePageLayoutStyleStore: vi.fn(),
}));

import { usePreviewStore } from '@/store/preview';
vi.mock('@/store/preview', () => ({
	usePreviewStore: vi.fn(),
}));

import { useThemenconfigStore } from '@/store/themenconfig';
vi.mock('@/store/themenconfig', () => ({
	useThemenconfigStore: vi.fn(),
}));

Object.defineProperty(window, 'open', { value: vi.fn() });
Object.defineProperty(window, 'scrollTo', { value: vi.fn() });
Object.defineProperty(window, 'ResizeObserver', { value: class ResizeObserver {
	observe () { /**/ }
	unobserve () { /**/ }
	disconnect () { /**/ }
} });

export function render (component, { props, provide, slots, stubs } = { stubs: {} }) {
	return libRender(component, {
		props,
		slots,
		global: {
			plugins: [
				createTestingPinia({
					createSpy: vi.fn,
				}),
				vuetifyPlugin,
			],
			provide,
			stubs: {
				'router-link': true,
				'router-view': true,
				'a-stepper': true,
				'a-stepper-step': true,
				'a-stepper-content': true,
				'a-treeview': true,
				'a-treeview-item': true,
				'v-dialog': VDialogStub,
				'v-select': VSelectStub,
				'v-btn': VBtnStub,
				...stubs,
			},
			mocks: {
				$t: translateFn,
			},
		},
	});
}
