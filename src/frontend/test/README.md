# Frontend testing
This guide widely follows the Vue.JS testing recommendations and substantiates them for the Admintool.

## Testing types

### Unit Testing
Unit testing is done as [in-source testing using vitest](https://vitest.dev/guide/in-source.html).
This covers the files in [the lib directory](../lib/).

### Store Testing
Store testing is done using separate vitest spec files.
Store testing should be unit-level testing and mock out helper functions from [lib](../lib/).
This covers the files in [the store directory](../store/) and [the test/store directory](./store/).

### Component Testing
Component testing is done using vitest and the Vue Testing Library, as recommended by Vue.JS.
Component testing should mock out stores and helper functions, but should render child component as intended.
(It may be necessary to mock out Vuetify components for cleaner tests, this is accepted.)
This covers the files in [the component directory](../component/) and [the test/component directory](./component/).

### E2E Testing
TODO: tbd

## Documentation

- [vitest](https://vitest.dev/guide)
- [Vue.JS testing recommendations](https://vuejs.org/guide/scaling-up/testing)
- [Pinia testing guide](https://pinia.vuejs.org/cookbook/testing)
- [Vue Testing Library](https://testing-library.com/docs/vue-testing-library/intro)
- [Vue Test Utils](https://test-utils.vuejs.org/guide)
