import { describe, it, expect } from 'vitest';
import { isNoElementTruthy } from './arrayHelper';

describe('isNoElementTruthy()', () => {
	it('should return true if array is undefined', () => {
		expect(isNoElementTruthy(undefined)).toBe(true);
	});
	it('should return true if array is empty', () => {
		expect(isNoElementTruthy([])).toBe(true);
	});
	it('should return true if all elements are not truthy', () => {
		const testArray = [
			undefined,
			null,
			NaN,
			undefined,
			false,
			0,
		];
		expect(isNoElementTruthy(testArray)).toBe(true);
	});
	it('should return false if one element is truthy', () => {
		const testArray = [
			undefined,
			null,
			NaN,
			207,
			undefined,
			false,
		];
		expect(isNoElementTruthy(testArray)).toBe(false);
	});
	it('should return false if multiple elements are truthy', () => {
		const testArray = [
			'moin',
			undefined,
			null,
			NaN,
			207,
			undefined,
			false,
			'hi',
			-42,
		];
		expect(isNoElementTruthy(testArray)).toBe(false);
	});
});
