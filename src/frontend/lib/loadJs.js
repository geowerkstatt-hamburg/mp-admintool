export async function loadJs (content, key) {
	const id = crypto.randomUUID();

	const rootEl = document.createElement('div');
	rootEl.style.display = 'none';
	const promise = new Promise(resolve => {
		window[id] = result => {
			delete window[id];
			rootEl.remove();
			resolve(result);
		};
	});

	const shadow = rootEl.attachShadow({ mode: 'closed' });
	const loader = document.createElement('script');
	loader.innerText = content;
	shadow.appendChild(loader);
	const getter = document.createElement('script');
	getter.innerText = `parent.window['${id}'](${key});`;
	shadow.appendChild(getter);

	document.body.appendChild(rootEl);
	return await promise;
}
