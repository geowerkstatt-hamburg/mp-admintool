import xml2json from '@/lib/xml2json';
import axios from 'axios';

const gfiSchemaRequest = {
	WMS: async layer => {
		const response = await axios.get(`${layer.url}?service=WMS&version=${layer.version}&request=GetFeatureInfoSchema&layers=${layer.layers}`)
			.then(({ request: { responseXML } }) => xml2json(responseXML));
		const result = response.schema.element.complexType.complexContent.extension.sequence.element;
		return result.map(line => line.getAttributes()?.name);
	},
	WFS: async layer => {
		const response = await axios.get(`${layer.url}?service=WFS&version=${layer.version}&request=DescribeFeatureType&typeNames=${layer.featureType}`)
			.then(({ request: { responseXML } }) => xml2json(responseXML));
		const elements = [ response.schema.element ].flat();
		return elements.filter(element => element.attributes.name === layer.featureType)
			.flatMap(element => element.complexType.complexContent.extension.sequence.element?.map(line => line.getAttributes()?.name) ?? []);
	},
};

export const supportedTypes = Object.keys(gfiSchemaRequest);

export async function getGfiSchema (layer) {
	if (gfiSchemaRequest[layer.typ]) {
		return await gfiSchemaRequest[layer.typ](layer);
	}
	return [];
}
