const prefixMap = {
	imageName: [ 'gml:PointPropertyType' ],
	circle: [ 'gml:PointPropertyType' ],
	polygon: [ 'gml:PolygonPropertyType', 'gml:SurfacePropertyType' ],
	lineStroke: [ 'gml:MultiLineStringPropertyType' ],
};

/**
 * Returns a list of GML types this styleConfig entry could (possibly) be applied to.
 *
 * @param {object} style entry from styleConfig
 * @returns {array} array of supported GML types
 */
export default function mapStyleGeometry (style) {
	return [
		'gml:GeometryPropertyType',
		...new Set(style.rules?.flatMap(({ style: styleInfo }) => Object.entries(prefixMap)
			.flatMap(([ prefix, types ]) => Object.keys(styleInfo).some(it => it.startsWith(prefix)) ? types : []))),
	];
}
