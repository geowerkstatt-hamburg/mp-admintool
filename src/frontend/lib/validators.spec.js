import { describe, it, expect } from 'vitest';
import { looksLikeConfigJson, stringIsInteger, stringIsFloat, isValidUrl } from './validators';

describe('looksLikeConfigJson()', () => {
	const validConfigJson = {
		Portalconfig: {
			treeType: 'light',
		},
		Themenconfig: {
			Fachdaten: {
				Layer: [],
			},
		},
	};
	it('detects a config.json object', () => {
		expect(looksLikeConfigJson(validConfigJson)).toBe(true);
	});
	it('allows additional root-level keys', () => {
		expect(looksLikeConfigJson({
			...validConfigJson,
			version: 'dev',
		})).toBe(true);
	});
	it('declines a config with missing keys', () => {
		expect(looksLikeConfigJson({ Portalconfig: {} })).toBe(false);
		expect(looksLikeConfigJson({})).toBe(false);
	});
	it('declines non-object configurations', () => {
		expect(looksLikeConfigJson(42)).toBe(false);
	});
});

describe('stringIsInteger()', () => {
	it('detects an positive integer', () => {
		expect(stringIsInteger('42')).toBe(true);
		expect(stringIsInteger('0')).toBe(true);
		expect(stringIsInteger('12313483754031707341170347043170347')).toBe(true);
	});
	it('detects an negative integer', () => {
		expect(stringIsInteger('-333333333333333333333333333333333333333333333')).toBe(true);
		expect(stringIsInteger('-5')).toBe(true);
		expect(stringIsInteger('-0')).toBe(true);
	});
	it('declines exponential integer strings', () => {
		expect(stringIsInteger('1e+53')).toBe(false);
		expect(stringIsInteger('+143')).toBe(false);
	});
	it('declines floating point numbers', () => {
		expect(stringIsInteger('1.1')).toBe(false);
		expect(stringIsInteger('123123,23')).toBe(false);
	});
	it('declines mixed strings and strings', () => {
		expect(stringIsInteger('112414134A')).toBe(false);
		expect(stringIsInteger('Horse')).toBe(false);
	});
});

describe('stringIsFloat()', () => {
	it('detects positive integers', () => {
		expect(stringIsFloat('0')).toBe(true);
		expect(stringIsFloat('222')).toBe(true);
	});
	it('detects positive floating point numbers', () => {
		expect(stringIsFloat('0.0')).toBe(true);
		expect(stringIsFloat('0.5')).toBe(true);
		expect(stringIsFloat('12313483754031707341170347043170347.1433333333333334341343413443')).toBe(true);
	});
	it('detects negative floating point numbers', () => {
		expect(stringIsFloat('-5')).toBe(true);
		expect(stringIsFloat('-5.4')).toBe(true);
	});
	it('declines exponential or non-pure-number strings', () => {
		expect(stringIsFloat('1e+53')).toBe(false);
		expect(stringIsFloat('+143.13')).toBe(false);
		expect(stringIsFloat('1.1D')).toBe(false);
		expect(stringIsFloat('112414134A')).toBe(false);
		expect(stringIsFloat('123123,23')).toBe(false);
		expect(stringIsFloat('Dinkel')).toBe(false);
	});
});

describe('isValidUrl()', () => {
	it('detects insecure URLs', () => {
		expect(isValidUrl('http://dataport.de')).toBe(true);
		expect(isValidUrl('http://www.land.bayern/der-freistaat?redirect=1')).toBe(true);
	});
	it('detects secure URLs', () => {
		expect(isValidUrl('https://www.masterportal.org/')).toBe(true);
	});
	it('declines invalid or incomplete URLs', () => {
		expect(isValidUrl('wss://www.masterportal.org')).toBe(false);
		expect(isValidUrl('httpss://www.masterportal.org')).toBe(false);
		expect(isValidUrl('http//www.masterportal.org')).toBe(false);
		expect(isValidUrl('www.masterportal.org')).toBe(false);
		expect(isValidUrl('www.hamburg.de')).toBe(false);
		expect(isValidUrl('DataportDinkelauflauf@dataport.de')).toBe(false);
	});
});
