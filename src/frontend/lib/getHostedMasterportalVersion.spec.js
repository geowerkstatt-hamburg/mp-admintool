import { describe, it, expect, beforeAll, afterEach, vi } from 'vitest';
import axios from 'axios';
import { versionToString, getMasterportalVersion, getHostedMasterportalVersion } from './getHostedMasterportalVersion';

describe('versionToString()', () => {
	it('converts version object into string', () => {
		expect(versionToString({ major: 1, minor: 2, patch: 3 })).toBe('1.2.3');
	});
	it('returns dev if dev is given as input', () => {
		expect(versionToString('dev')).toBe('dev');
	});
});

describe('getMasterportalVersion()', () => {
	it('returns a version object if a valid version string is given', () => {
		expect(getMasterportalVersion('v2.3.324')).toEqual({ major: 2, minor: 3, patch: 324 });
		expect(getMasterportalVersion('2.19.0')).toEqual({ major: 2, minor: 19, patch: 0 });
	});
	it('returns undefined if version is invalid', () => {
		expect(getMasterportalVersion('perfect')).toBeUndefined();
	});
});

describe('getHostedMasterportalVersion()', () => {
	beforeAll(() => {
		axios.get = vi.fn();
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('detects tagged version 2.16.0', async () => {
		axios.get.mockResolvedValueOnce({
			data: '<script type="text/javascript" src="../mastercode/2_16_0/js/masterportal.js"></script>',
		});
		expect(await getHostedMasterportalVersion('dummyPortal')).toEqual({ major: 2, minor: 16, patch: 0 });
	});
	it('returns undefined if version could not be detected', async () => {
		axios.get.mockResolvedValueOnce({
			data: '<script type="text/javascript" src="../../build/js/masterportal.js"></script>',
		});
		expect(await getHostedMasterportalVersion('dummyPortal')).toBeUndefined();
	});
});
