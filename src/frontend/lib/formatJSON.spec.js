import { describe, it, expect } from 'vitest';
import { formatJSON } from './formatJSON';

describe('formatJSON()', () => {
	const obj = {
		object: { data: 'port' },
		array: [ 'Daten', 'Hafen' ],
		number: 42,
	};
	it('prints valid JSON', () => {
		expect(JSON.parse(formatJSON(obj))).toEqual(obj);
	});
	it('uses tabs for indentation', () => {
		expect(formatJSON(obj).includes('\t')).toBe(true);
	});
});
