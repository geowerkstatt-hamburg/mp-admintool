import { describe, it, expect } from 'vitest';
import deepCopy from './deepCopy';

describe('deepCopy()', () => {
	it('copies the target deep', () => {
		const obj = {
			level: {
				ist: {
					auch: {
						nur: [
							'eine',
							'Zahl',
							{
								drei: 'zum Beispiel',
							},
						],
					},
				},
			},
		};
		const copy = deepCopy(obj);
		expect(copy).not.toBe(obj);
		expect(copy).toEqual(obj);
		obj.level.ist.auch.nur[2].drei = 'oder?';
		expect(copy).not.toEqual(obj);
	});
});
