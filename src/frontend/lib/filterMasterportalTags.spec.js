import { describe, it, expect } from 'vitest';
import { versionGreaterOrEqualTo } from './filterMasterportalTags';

describe('versionGreaterOrEqualTo()', () => {
	it('returns true if version is greater or equal to comparator', () => {
		const comparator = [ 2, 5, 7 ];
		expect(versionGreaterOrEqualTo('v1.0.0', comparator)).toBe(false);
		expect(versionGreaterOrEqualTo('v1.9.9', comparator)).toBe(false);
		expect(versionGreaterOrEqualTo('v2.5.6', comparator)).toBe(false);
		expect(versionGreaterOrEqualTo('v2.5', comparator)).toBe(false);
		expect(versionGreaterOrEqualTo('v2', comparator)).toBe(false);
		expect(versionGreaterOrEqualTo('v2.5.6.9', comparator)).toBe(false);
		expect(versionGreaterOrEqualTo('v2.5.7', comparator)).toBe(true);
		expect(versionGreaterOrEqualTo('v3', comparator)).toBe(true);
		expect(versionGreaterOrEqualTo('v2.6', comparator)).toBe(true);
		expect(versionGreaterOrEqualTo('v2.6.7.9', comparator)).toBe(true);
	});
});
