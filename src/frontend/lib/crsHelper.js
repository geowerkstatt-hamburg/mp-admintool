import { crs } from '@masterportal/masterportalapi';

export function getLabels (epsgCode) {
	const projection = crs.getProjection(epsgCode);
	if (projection) {
		if (projection.projName === 'longlat') {
			// For projName longlat unit is implicitly degrees
			return {
				labelX: [ 'controls.coordinate.label.longLatX' ],
				labelY: [ 'controls.coordinate.label.longLatY' ],
			};
		}
		return {
			labelX: [ 'controls.coordinate.label.unitX', { unit: projection.units } ],
			labelY: [ 'controls.coordinate.label.unitY', { unit: projection.units } ],
		};
	}

	// undefined projection
	return {
		labelX: [ 'controls.coordinate.label.otherX' ],
		labelY: [ 'controls.coordinate.label.otherY' ],
	};
}
