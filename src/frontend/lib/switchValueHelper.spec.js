import { describe, it, expect } from 'vitest';
import { switchIsON } from './switchValueHelper';

describe('switchIsON()', () => {
	it.each([
		[ 1, true ],
		[ '1', true ],
		[ true, true ],
		[ 'true', true ],
		[ 'yes', true ],
		[ 'YES', true ],
		[ 0, false ],
		[ '0', false ],
		[ false, false ],
		[ 'false', false ],
		[ 'no', false ],
		[ 'NO', false ],
	])('detects %s as %s', (value, expected) => {
		expect(switchIsON(value)).toBe(expected);
	});
});
