import { describe, it, expect } from 'vitest';
import resolvePath from './resolvePath';

describe('resolvePath()', () => {
	it('returns target reference for given path', () => {
		const sym = Symbol();
		expect(resolvePath({ Portalconfig: { tools: { admin: sym } } }, [ 'Portalconfig', 'tools', 'admin' ])).toBe(sym);
	});
	it('returns undefined if path does not exist', () => {
		expect(resolvePath({ Portalconfig: {} }, [ 'Portalconfig', 'tools', 'admin' ])).toBeUndefined();
	});
});
