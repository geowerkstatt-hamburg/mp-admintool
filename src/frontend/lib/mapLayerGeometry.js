import { describeFeatureType, getFeatureDescription } from '@/lib/describeFeatureType';

/**
 * Returns a list of GML types this layer's features delivers.
 *
 * @param {object} layer layer
 * @returns {array} array of supported GML types
 */
export default async function mapLayerGeometry (layer) {
	try {
		const featureTypeDescription = await describeFeatureType(layer.url, layer.version, layer.featureType);
		const featureDescription = getFeatureDescription(featureTypeDescription, layer.featureType);
		return [ ...new Set(featureDescription.map(({ type }) => type).filter(type => type.startsWith('gml:'))) ];
	} catch (e) {
		console.error(e);
		return [ 'gml:GeometryPropertyType' ];
	}
}
