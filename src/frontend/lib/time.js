export function getTimestamp (date = new Date()) {
	return [
		date.getFullYear().toString(),
		...[
			date.getMonth() + 1,
			date.getDate(),
			date.getHours(),
			date.getMinutes(),
			date.getSeconds(),
		].map(it => String(it).padStart(2, '0')),
	].join('');
}
