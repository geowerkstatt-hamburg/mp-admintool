// $t escapes some characters from json.stringify files
//  DOMParser creates readable string
export function beautifyString (s) {
	if (s && typeof s === 'string') {
		const doc = new DOMParser().parseFromString(s, 'text/html');
		return doc.documentElement.textContent;
	}
	return '';
}
