import { describe, it, expect } from 'vitest';
import { upperFirst } from './stringHelper';

describe('upperFirst()', () => {
	it.each([
		[ 'dataport', 'Dataport' ],
		[ 'chapters Store', 'Chapters Store' ],
		[ 'Dataport', 'Dataport' ],
	])('turns %s into %s', (input, expected) => {
		expect(upperFirst(input)).toBe(expected);
	});
	it('returns empty string if given empty string', () => {
		expect(upperFirst('')).toBe('');
	});
});
