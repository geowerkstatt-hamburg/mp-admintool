/**
 * Capitalize the first letter of given string.
 *
 * @param {String} string string whose first letter should be capitalized
 * @returns {String} same string with capitalized first letter
 */
export function upperFirst (string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}
