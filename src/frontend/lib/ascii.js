export function asciify (text) {
	return text
		.replaceAll('ä', 'ae')
		.replaceAll('Ä', 'Ae')
		.replaceAll('Ü', 'Ue')
		.replaceAll('ü', 'ue')
		.replaceAll('Ö', 'Oe')
		.replaceAll('ö', 'oe')
		.replaceAll('ß', 'ss')
		.replaceAll(/[^a-z\d]+([a-z\d])/gi, (v, m) => m.toUpperCase());
}
