import { describe, beforeEach, afterEach, it, expect, vi } from 'vitest';
import showMap from './showMap';

import { useConfigStore } from '@/store/config';
vi.mock('@/store/config', () => ({
	useConfigStore: vi.fn(),
}));

import { useLayersStore } from '@/store/layers';
vi.mock('@/store/layers', () => ({
	useLayersStore: vi.fn(),
}));

import { useAlertStore } from '@/store/alert';
vi.mock('@/store/alert', () => ({
	useAlertStore: vi.fn(),
}));

import { getBasePath } from '@/lib/basePath';
vi.mock('@/lib/basePath', () => ({
	getBasePath: vi.fn(),
}));

import { crs, setBackgroundImage } from '@masterportal/masterportalapi';
vi.mock('@masterportal/masterportalapi', () => ({
	crs: {
		transform: vi.fn(),
	},
	setBackgroundImage: vi.fn(),
}));

import api from '@masterportal/masterportalapi/src/maps/api';
vi.mock('@masterportal/masterportalapi/src/maps/api', () => ({
	default: {
		map: {
			createMap: vi.fn(),
		},
	},
}));

import createStyle from '@masterportal/masterportalapi/src/vectorStyle/createStyle';
vi.mock('@masterportal/masterportalapi/src/vectorStyle/createStyle', () => ({
	default: {
		createStyle: vi.fn(),
	},
}));

beforeEach(ctx => {
	useConfigStore.mockReturnValue({
		onInitialized: vi.fn(cb => {
			cb();
		}),
		masterportalApiConfig: {
			epsg: 'EPSG:25832',
			layers: [ { id: 'BASEMAP' } ],
			extraConfig: true,
			extent: [ 1, 2, 3, 4 ],
			startCenter: [ 21, 22 ],
			wfsImgPath: 'WFSPATH',
		},
		styleConf: [
			{
				styleId: 'STYLE',
				fancy: true,
			},
		],
	});
	useLayersStore.mockReturnValue({
		getLayerDetails: vi.fn((ids) => Promise.resolve(ids.map(id => ({ id, _test: true })))),
	});
	api.map.createMap.mockImplementation(mapConfig => {
		ctx.mapConfig = mapConfig;
		ctx.addLayer = vi.fn();
		return {
			_test: true,
			addLayer: ctx.addLayer,
		};
	});
	createStyle.createStyle.mockImplementation((style, feature, isCluster, path) => ({ style, feature, path }));
	crs.transform.mockImplementation((sourceEpsg, targetEpsg, coordinates) => coordinates.map(coord => -coord));
});

afterEach(() => {
	vi.restoreAllMocks();
});

describe('showMap()', () => {
	it('shows a map', async ctx => {
		await expect(showMap('TEST', null, 'EPSG:25832')).resolves.toHaveProperty('_test');
		expect(ctx.mapConfig).toHaveProperty('layerConf', [
			{ id: 'BASEMAP', _test: true },
			{ id: 'TEST', _test: true },
		]);
		expect(ctx.mapConfig).toHaveProperty('extraConfig');
		expect(ctx.mapConfig).toHaveProperty('extent', [ 1, 2, 3, 4 ]);
		expect(setBackgroundImage).toHaveBeenCalled();
		expect(ctx.addLayer).toHaveBeenCalledWith('TEST');
	});
	it('transforms coordinates if different EPSG code is given', async ctx => {
		await showMap('TEST', null, 'EPSG:3857');
		expect(ctx.mapConfig).toHaveProperty('extent', [ -1, -2, -3, -4 ]);
		expect(ctx.mapConfig).toHaveProperty('startCenter', [ -21, -22 ]);
	});
	it('uses a given styleId and generates the style', async ctx => {
		await showMap('TEST', 'STYLE', 'EPSG:3857');
		const styleFn = ctx.mapConfig.layerConf?.[1]?.style;
		expect(styleFn).toBeTypeOf('function');
		const featureStub = {
			_test: true,
			get: () => null,
		};
		expect(styleFn(featureStub)).toEqual({
			style: {
				styleId: 'STYLE',
				fancy: true,
			},
			feature: featureStub,
			path: 'WFSPATH',
		});
	});
});
