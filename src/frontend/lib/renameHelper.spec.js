import { describe, it, expect } from 'vitest';
import { renameLabels } from './renameHelper';

describe('renameLabels', () => {
	const graph = {
		de: {
			label: 'renameMe',
			items: [
				{ label: 'renameMeTwo' },
				{ label: 'dontRenameMe' },
				{
					nested: {
						nested: {
							nested: {
								label: 'renameMeThree',
							},
						},
					},
				},
			],
		},
		en: {
			label: 'renameMe',
			items: [
				{ label: 'renameMeTwo' },
				{ label: 'dontRenameMe' },
				{
					nested: {
						nested: {
							nested: {
								label: 'renameMeThree',
							},
						},
					},
				},
			],
		},
	};
	const renameMatrix = {
		renameMe: 'itRenamedMe',
		renameMeTwo: 'itRenamedMeTwo',
		renameMeThree: 'itRenamedMeThree',
	};

	it('renames labels deep and does not touch other keys', () => {
		renameLabels(graph, renameMatrix);
		expect(graph.de.label).toBe('itRenamedMe');
		expect(graph.en.label).toBe('itRenamedMe');
		expect(graph.de.items[0].label).toBe('itRenamedMeTwo');
		expect(graph.en.items[0].label).toBe('itRenamedMeTwo');
		expect(graph.de.items[2].nested.nested.nested.label).toBe('itRenamedMeThree');
		expect(graph.en.items[2].nested.nested.nested.label).toBe('itRenamedMeThree');
		expect(graph.de.items[1].label).toBe('dontRenameMe');
		expect(graph.en.items[1].label).toBe('dontRenameMe');
	});
});
