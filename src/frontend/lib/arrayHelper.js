/**
 * Check the truthiness of an array and its elements
 * @param {Array} array - Array to be tested
 * @returns {Boolean} True if the provided array is not truthy or no element is truthy
 */
export function isNoElementTruthy (array) {
	if (array) {
		return typeof array.find(item => item) === 'undefined';
	}
	return true;
}
