import { isNoElementTruthy } from './arrayHelper';
import { useAlertStore } from '@/store/alert';
import crosshairs from '@images/crosshairs-gps.png';

import { Vector as VectorSource } from 'ol/source';
import { unByKey } from 'ol/Observable';
import VectorLayer from 'ol/layer/Vector';
import { Fill, Stroke, Style, Icon, Circle as CircleStyle } from 'ol/style';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import Polygon from 'ol/geom/Polygon';
import { Draw, Modify, Snap } from 'ol/interaction';

// TODO: This file hasn't been fully checked to be compliant with implicit and explicit Vue 3 conventions yet.
// TODO: It also has not been migrated to the new coding style completely yet...
/* eslint-disable */

export default class {
	constructor (map, type, coordinates, onModificationEnds) {
		this.alert = useAlertStore();
		this.map = map;
		this.type = type;

		this.inputSource = new VectorSource();
		this.helperSource = new VectorSource();

		this.changeHandle = null;

		this.crosshairsStyle = new Style({
			image: new Icon({
				src: crosshairs
			})
		});

		this.circleStyle = new Style({
			image: new CircleStyle({
				radius: 5,
				fill: new Fill({
					color: 'red'
				})
			})
		});

		this.initialize(coordinates, onModificationEnds);

	}

	initialize (coordinates, onModificationEnds) {
		const vector = new VectorLayer({
			zIndex: 2,
			source: this.inputSource,
			style: this.type === 'point' ? this.crosshairsStyle : this.circleStyle
		}),
		modify = new Modify({
			source: this.inputSource,
			style: this.type === 'point' ? this.crosshairsStyle : null
		});

		this.map.addLayer(vector);

		if (onModificationEnds) {
			modify.on('modifyend', onModificationEnds);
		}
		this.map.addInteraction(modify);

		this.map.addInteraction(new Snap({
			source: this.inputSource
		}));

		if (this.type === 'box') {
			this.map.addLayer(new VectorLayer({
				source: this.helperSource,
				style: new Style({
					stroke: new Stroke({
						color: 'blue',
						width: 3
					}),
					fill: new Fill({
						color: 'rgba(0,0,255,0.1)'
					})
				})
			}));
		}

		// Start with input mode if coordinates are invalid (missing)
		if (isNoElementTruthy(coordinates)) {
			this.create();
		} else {
			this.load(coordinates);
		}
	}

	load (coordinates) {
		const crosshairsFeature = new Feature({
			geometry: new Point(coordinates),
			type: 'point'
		});
		let zoomToGeometry;
		// TODO: Check if coordinates are valid
		switch (this.type) {
			case 'point':
				this.inputSource.addFeature(crosshairsFeature);
				zoomToGeometry = this.inputSource.getFeatures()[0].getGeometry();
				break;
			case 'box':
				// Coordinates: [West South East North]
				this.inputSource.addFeature(new Feature(new Point([coordinates[0], coordinates[3]])));
				this.inputSource.addFeature(new Feature(new Point([coordinates[2], coordinates[1]])));

				this.renderBox();

				zoomToGeometry = this.helperSource.getFeatures()[0].getGeometry();

				// Rerender box every time the corner nodes are changeing
				this.changeHandle = this.inputSource.on('change', this.renderBox.bind(this));

				break;
			default:
				console.error('Unknown geometry!');
				return;
		}

		this.map.getView().fit(zoomToGeometry, {
			maxZoom: 7
		});
	}

	update (coordinates) {
		switch (this.type) {
			case 'point':
				this.inputSource.getFeatures()[0].setGeometry(new Point(coordinates));
				break;
			case 'box':
				unByKey(this.changeHandle);

				// Coordinates: [West South East North]
				this.inputSource.clear();
				this.inputSource.addFeature(new Feature(new Point([coordinates[0], coordinates[3]])));
				this.inputSource.addFeature(new Feature(new Point([coordinates[2], coordinates[1]])));

				this.renderBox();
				// Rerender box every time the corner nodes are changeing
				this.changeHandle = this.inputSource.on('change', this.renderBox.bind(this));

				break;
			default:
				console.error('Unknown geometry!');
				break;
		}
	}

	reRender () {
		this.map.updateSize();
	}

	renderBox (evt) {
		const firstCorner = this.inputSource.getFeatures()[0].getGeometry().getCoordinates();
		let secondCorner;

		if (this.inputSource.getFeatures().length === 2) {
			secondCorner = this.inputSource.getFeatures()[1].getGeometry().getCoordinates();
		}
		else {
			secondCorner = evt.coordinate;
		}

		this.helperSource.clear();
		this.helperSource.addFeature(new Feature(new Polygon([
			[
				[
					firstCorner[0],
					firstCorner[1]
				],
				[
					firstCorner[0],
					secondCorner[1]
				],
				[
					secondCorner[0],
					secondCorner[1]
				],
				[
					secondCorner[0],
					firstCorner[1]
				],
				[
					firstCorner[0],
					firstCorner[1]
				]
			]
		])));
	}

	reCreate () {
		if (this.type === 'box') {
			unByKey(this.changeHandle);
		}

		this.inputSource.clear();
		this.helperSource.clear();

		return this.create();
	}

	create () {

		/* eslint-disable-next-line no-unused-vars */
		return new Promise((resolve, reject) => {

			const styleForCrosshairs = this.crosshairsStyle,
			draw = new Draw({
				source: this.inputSource,
				type: 'Point',
				style: this.type === 'point' ? styleForCrosshairs : null
			});

			this.map.addInteraction(draw);

			this.inputSource.once('change', () => {
				if (this.type === 'point') {
					this.map.removeInteraction(draw);
					resolve();
				}
				else if (this.type === 'box') {

					// Trigger rerender of the box for every possible new edge position
					const handle = this.map.on('pointermove', this.renderBox.bind(this));

					// Stop input-mode as soon as two edges are defined
					this.inputSource.once('change', () => {
						unByKey(handle);
						this.map.removeInteraction(draw);

						this.changeHandle = this.inputSource.on('change', this.renderBox.bind(this));
						resolve();
					});

				}
			});
		});
	}

	getCoordinates () {

		if (this.type === 'point') {

			if (this.inputSource.getFeatures().length !== 1) {
				/* eslint-disable-next-line no-undefined */
				return undefined;
			}
			return this.inputSource.getFeatures()[0].getGeometry().getCoordinates();
		}
		else if (this.type === 'box') {

			if (this.inputSource.getFeatures().length !== 2) {
				/* eslint-disable-next-line no-undefined */
				return undefined;
			}

			const firstCorner = this.inputSource.getFeatures()[0].getGeometry().getCoordinates(),
					secondCorner = this.inputSource.getFeatures()[1].getGeometry().getCoordinates(),
					coordinates = Array(4);

			if (firstCorner[0] === secondCorner[0] || firstCorner[1] === secondCorner[1]) {
				console.error('Ungültige Eingabe: Die Eckpunkte der Box müssen unterschiedliche Koordinaten haben.');
				// TODO: Localize this message
				this.alert.error({ content: 'Ungültige Eingabe: Die Eckpunkte der Box müssen unterschiedliche Koordinaten haben.' });
				/* eslint-disable-next-line no-undefined */
				return undefined;
			}

			// Sort the cooordinates to get a array with [west south east north]
			if (firstCorner[0] > secondCorner[0]) {
				coordinates[0] = secondCorner[0];
				coordinates[2] = firstCorner[0];
			}
			else {
				coordinates[0] = firstCorner[0];
				coordinates[2] = secondCorner[0];
			}

			if (firstCorner[1] < secondCorner[1]) {
				coordinates[1] = firstCorner[1];
				coordinates[3] = secondCorner[1];
			}
			else {
				coordinates[1] = secondCorner[1];
				coordinates[3] = firstCorner[1];
			}

			return coordinates;
		}

		/* eslint-disable-next-line no-undefined */
		return undefined;
	}
}
