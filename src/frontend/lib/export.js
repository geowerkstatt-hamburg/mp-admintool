import { computed } from 'vue';
import { useAlertStore } from '@/store/alert';
import { useConfigStore } from '@/store/config';
import { useGraphStore } from '@/store/graph';
import { getBasePath } from '@/lib/basePath';
import { formatJSON } from '@/lib/formatJSON';
import { versionToString } from '@/lib/getHostedMasterportalVersion';
import { versionGreaterOrEqualTo } from '@/lib/filterMasterportalTags';
import axios from 'axios';

const alert = computed(() => useAlertStore());
const config = computed(() => useConfigStore());
const graph = computed(() => useGraphStore());

const adminBackendURL = computed(() => location.origin + getBasePath());
export const adminBackendConfigURL = computed(() => `${adminBackendURL.value}/configs/`);
export const adminBackendBundleURL = computed(() => `${adminBackendURL.value}/masterportalBundle/${graph.value.version}`);

export const exportConfigJSON = computed(() => ({
	...graph.value.config,
	version: graph.value.version,
}));
export const formattedConfigJSON = computed(() => formatJSON(exportConfigJSON.value));

export const disablePreview = computed(() => !(
	(config.value.layerConfUrl === config.value.admintoolConfig.layerConf &&
	config.value.styleConfUrl === config.value.admintoolConfig.styleConf &&
	config.value.restConfUrl === config.value.admintoolConfig.restConf) ||
	config.value.hostedMasterportalURL !== config.value.admintoolConfig.hostedMasterportalURL
));

const hostedMasterportalVersion = computed(() => config.value.hostedMasterportalVersion);
const hostedMasterportalURL = computed(() => config.value.hostedMasterportalURL);
const hostedMasterportalURLRouted = computed(() => config.value.admintoolConfig?.hostedMasterportalURLRouted);
const hostedMasterportalVersionString = computed(() => hostedMasterportalVersion.value ? versionToString(hostedMasterportalVersion.value) : '?');
const configKeyName = computed(() => hostedMasterportalVersion.value && versionGreaterOrEqualTo(hostedMasterportalVersionString.value, [ 3, 0, 0 ]) ? 'jsonConfig' : 'config');

export function getMasterportalPreviewURL (configURL) {
	return `${hostedMasterportalURL.value}/?${configKeyName.value}=${configURL}`;
}

export function getMasterportalPermanentURL (configURL, configName) {
	if (!hostedMasterportalURLRouted.value || !configName) {
		return getMasterportalPreviewURL(configURL);
	}
	return `${hostedMasterportalURLRouted.value}/${configName}`;
}

export async function uploadConfig (configName = '', overwrite = false) {
	try {
		const { data } = await axios.post(`${adminBackendConfigURL.value}${configName}${overwrite ? '?overwrite=1' : ''}`, exportConfigJSON.value);
		return `${adminBackendConfigURL.value}${data}.json`;
	} catch (err) {
		console.warn(err);
		alert.value.error({ content: err.response.data || 'chapters.export.errors.publishError' });
		if (err.response.data === 'errors.nameDuplicate') {
			throw err;
		}
	}
	return false;
}
