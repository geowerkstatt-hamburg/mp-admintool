import { describe, it, expect } from 'vitest';
import { getTimestamp } from './time';

describe('getTimestamp()', () => {
	it('converts the date of the first commit correctly', () => {
		expect(getTimestamp(new Date('Fri Aug 17 07:05:43 2018'))).toBe('20180817070543');
	});
});
