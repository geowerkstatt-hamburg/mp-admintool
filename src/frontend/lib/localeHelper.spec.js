import { describe, it, expect } from 'vitest';
import { beautifyString } from './localeHelper';

describe('beautifyString()', () => {
	it('should return "" if argument is no string', () => {
		expect(beautifyString()).toBe('');
		expect(beautifyString(3)).toBe('');
		expect(beautifyString({ test: 'test' })).toBe('');
	});
	it('should return "" if argument is ""', () => {
		expect(beautifyString('')).toBe('');
	});
	it('should return identity if the string does not contain escapes', () => {
		const str = 'Hello, this is a test; iit; works';
		expect(beautifyString(str)).toBe(str);
	});
	it('should return readable string if argument contains escaped characters', () => {
		const str = '&quot;Hello&quot;&#x2F;Hallo, this is a test; iit; works';
		const res = '"Hello"/Hallo, this is a test; iit; works';
		expect(beautifyString(str)).toBe(res);
	});
});
