import { describe, it, expect } from 'vitest';
import { asciify } from './ascii';

describe('asciify()', () => {
	it('returns an ASCII letter word as-is', () => {
		expect(asciify('dataport')).toBe('dataport');
		expect(asciify('GeoWerkstatt')).toBe('GeoWerkstatt');
	});
	it('turns a dashed word into camel case', () => {
		expect(asciify('landesbetrieb-42-geoinformation-und-vermessung')).toBe('landesbetrieb42GeoinformationUndVermessung');
	});
	it('replaces Umlaute with ASCII characters', () => {
		expect(asciify('Übergrößenänderungsanträge')).toBe('Uebergroessenaenderungsantraege');
	});
});
