import { computed } from 'vue';
import { useConfigStore } from '@/store/config';
import { useLayersStore } from '@/store/layers';
import mapStyleGeometry from '@/lib/mapStyleGeometry';

const config = computed(() => useConfigStore());
const layers = computed(() => useLayersStore());

export const typeConfigMap = {
	layerConf: {
		url: computed({
			get: () => config.value.layerConfUrl,
			set: newUrl => {
				config.value.layerConfUrl = newUrl;
			},
		}),
		defaultUrl: computed(() => config.value.admintoolConfig.layerConf),
		content: computed(() => false),
		load: (...args) => layers.value.loadLayers(...args),
		description: computed(() => layers.value.count),
		validationProps: [
			'id', 'name', 'typ',
		],
		validationTypes: [
			'WMS', 'WMTS', 'WFS', 'VectorTile', 'SensorThings', 'GeoJSON',
			'OAF', 'Heatmap', 'Tileset3D', 'Terrain3D', 'Oblique', 'Entities3D',
		],
	},
	restConf: {
		url: computed({
			get: () => config.value.restConfUrl,
			set: newUrl => {
				config.value.restConfUrl = newUrl;
			},
		}),
		defaultUrl: computed(() => config.value.admintoolConfig.restConf),
		content: computed(() => config.value.restConf),
		load: conf => {
			config.value.restConf = conf;
		},
		description: computed(() => config.value.restConf.length),
		validationProps: [
			'id', 'name', 'typ', 'url',
		],
	},
	styleConf: {
		url: computed({
			get: () => config.value.styleConfUrl,
			set: newUrl => {
				config.value.styleConfUrl = newUrl;
			},
		}),
		defaultUrl: computed(() => config.value.admintoolConfig.styleConf),
		content: computed(() => config.value.styleConf),
		load: conf => {
			config.value.styleConf = conf;
		},
		description: computed(() => Object.entries(config.value.styleConf
			.flatMap(mapStyleGeometry)
			.reduce((acc, it) => {
				acc[it] = (acc[it] ?? 0) + 1;
				return acc;
			}, {}))
			.map(([ key, value ]) => `${key === 'gml:GeometryPropertyType' ? 'Total' : key.substring(4, key.length - 12)}: ${value}`)
			.join('; ')),
		validationProps: [
			'styleId', 'rules',
		],
	},
};

export async function load (key, data) {
	const typeConfig = typeConfigMap[key];
	if (!Array.isArray(data) || !data.length) {
		throw new Error({ content: 'chapters.loading.jsons.errorFormat' });
	}
	typeConfig.validationProps
		.filter(property => !data[0].hasOwnProperty(property))
		.forEach(property => {
			throw new Error({ content: 'chapters.loading.jsons.missingProp', options: { property } });
		});
	await typeConfig.load(data);
}
