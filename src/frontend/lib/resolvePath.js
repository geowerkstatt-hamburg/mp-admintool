export default function resolvePath (obj, path) {
	return path.reduce((pos, part) => pos?.[part], obj);
}
