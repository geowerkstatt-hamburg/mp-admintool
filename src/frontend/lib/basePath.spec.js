import { describe, it, expect, afterEach, vi } from 'vitest';
import { getBasePath } from './basePath';

describe('getBasePath()', () => {
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('returns base path if set', () => {
		vi.spyOn(Storage.prototype, 'getItem').mockReturnValue('BASEPATH');
		expect(getBasePath()).toBe('BASEPATH');
	});
	it('returns an empty string if no base path is set', () => {
		vi.spyOn(Storage.prototype, 'getItem').mockReturnValue(null);
		expect(getBasePath()).toBe('');
	});
});
