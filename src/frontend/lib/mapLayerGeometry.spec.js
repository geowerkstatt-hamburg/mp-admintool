import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { describeFeatureType, getFeatureDescription } from '@/lib/describeFeatureType';
import mapLayerGeometry from './mapLayerGeometry';

vi.mock('@/lib/describeFeatureType', () => ({
	describeFeatureType: vi.fn(),
	getFeatureDescription: vi.fn(),
}));

describe('mapLayerGeometry()', () => {
	function generateSchema (featureTypes) {
		return { mockedSchema: featureTypes };
	}
	const gmlTypes = {
		unknown: 'gml:GeometryPropertyType',
		point: 'gml:PointPropertyType',
		line: 'gml:MultiLineStringPropertyType',
		polygon: 'gml:PolygonPropertyType',
	};
	const mockRequestMap = {
		trivial: generateSchema(Object.entries(gmlTypes).map(([ name, type ]) => ({
			name,
			attributes: [ 'name', 'label', 'hausnummer', type ],
		}))),
		singlePoint: generateSchema([
			{
				name: 'singleFeature',
				attributes: [ gmlTypes.point ],
			},
		]),
		multiple: generateSchema([
			{
				name: 'multiFeature',
				attributes: [ gmlTypes.point, 'blub', gmlTypes.line, gmlTypes.polygon ],
			},
		]),
	};

	beforeEach(() => {
		describeFeatureType.mockImplementation(url => {
			if (mockRequestMap[url]) {
				return Promise.resolve(mockRequestMap[url]);
			}
			throw new Error('Mocked network failure');
		});
		getFeatureDescription.mockImplementation((json, featureTypeName) => json.mockedSchema
			.find(({ name }) => name === featureTypeName)?.attributes.map(type => ({ type })));
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});

	async function test (layer, ...result) {
		expect(await mapLayerGeometry(layer)).toEqual(result.map(type => gmlTypes[type]));
	}

	it.each(Object.keys(gmlTypes).map(v => [ v ]))('detects simple layer and feature of type %s', async featureType => {
		await test({ url: 'trivial', featureType }, featureType);
	});
	it('supports non-array descriptions', async () => {
		await test({ url: 'singlePoint', featureType: 'singleFeature' }, 'point');
	});
	it('supports layers with multiple draw options', async () => {
		await test({ url: 'multiple', featureType: 'multiFeature' }, 'point', 'line', 'polygon');
	});
	it('returns unknown for unreachable services', async () => {
		vi.spyOn(console, 'error').mockImplementation(() => { /**/ });
		await test({
			url: 'this_url_does_not_contain_any_typo_no_raelly_not',
			featureType: 'but_at_least_thiß_feature_name_is_typo_fee',
		}, 'unknown');
		expect(console.error).toHaveBeenCalled();
	});
});
