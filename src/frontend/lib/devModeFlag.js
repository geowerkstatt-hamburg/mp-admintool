import { switchIsON } from '@/lib/switchValueHelper';

export const devModeFlag = import.meta.env.DEV;
export const useLocalConfig = switchIsON(import.meta.env.VITE_USE_LOCAL_CONFIG_FILES);
