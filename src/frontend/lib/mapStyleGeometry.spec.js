import { describe, it, expect } from 'vitest';
import mapStyleGeometry from './mapStyleGeometry';

describe('mapStyleGeometry()', () => {
	const gmlTypes = {
		unknown: 'gml:GeometryPropertyType',
		point: 'gml:PointPropertyType',
		line: 'gml:MultiLineStringPropertyType',
		polygon: 'gml:PolygonPropertyType',
		surface: 'gml:SurfacePropertyType',
	};
	function test (style, ...result) {
		expect(mapStyleGeometry(style)).toEqual(result.map(type => gmlTypes[type]));
	}

	it('detects imageName property as a point', () => {
		test({
			styleId: 'test1',
			rules: [ { style: { imageName: 'admintool.png' } } ],
		}, 'unknown', 'point');
	});
	it('detects circleRadius property as a point', () => {
		test({
			styleId: 'test2',
			rules: [ { style: { circleRadius: 10 } } ],
		}, 'unknown', 'point');
	});
	it('detects lineStrokeWidth property as a line', () => {
		test({
			styleId: 'test3',
			rules: [ { style: { lineStrokeWidth: 5 } } ],
		}, 'unknown', 'line');
	});
	it('detects polygonFillColor property as a polygon', () => {
		test({
			styleId: 'test4',
			rules: [ { style: { polygonFillColor: [ 10, 200, 100, 0.42 ] } } ],
		}, 'unknown', 'polygon', 'surface');
	});
	it('detects unknown for any style', () => {
		test({}, 'unknown');
	});
	it('detects multiple types if given', () => {
		test({
			styleId: 'test6',
			rules: [
				{ style: { circleRadius: 37 } },
				{ style: { polygonFillColor: [ 10, 200, 100, 0.42 ] } },
			],
		}, 'unknown', 'point', 'polygon', 'surface');
	});
});
