import { createPinia } from 'pinia';
import persistPlugin from './pinia/persist';

const pinia = createPinia();
pinia.use(persistPlugin);
export default pinia;
