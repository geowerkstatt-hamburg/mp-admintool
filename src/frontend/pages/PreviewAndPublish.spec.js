import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { render } from '@/test/components/helper';

import PreviewAndPublish from '@/pages/PreviewAndPublish.vue';

import { useConfigStore } from '@/store/config';

vi.mock('@/lib/export', () => ({
	disablePreview: vi.fn(() => false),
}));

vi.mock('@/lib/devModeFlag', () => ({
	devModeFlag: false,
}));

describe('PreviewAndPublish', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		useConfigStore.mockReturnValue({
			admintoolConfig: {
				hostedMasterportalURL: 'HOST',
			},
			hostedMasterportalURL: 'HOST',
			isExcludedFeature: () => false,
		});
		ctx.render = render(PreviewAndPublish, {
			stubs: {
				ExportPreview: true,
				ExportPublish: true,
				ExportDownloadFull: true,
				ExportDownloadJSON: true,
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('renders introduction text', ({ render: { getByText } }) => {
		getByText('{$t(chapters.export.paragraph)}');
	});
});

/*
TODO: Migrate these legacy tests to component tests for the corresponding @/components/Export/*.vue components

    beforeEach(() => {
        getVersion = jest.fn(() => "dummyVersion");
        getHostedMasterportalVersion = jest.fn(() => ({
            major: "2",
            minor: "0",
            patch: "0"
        }));

        vuetify = new Vuetify();

        // Prepare database with dummy data. Use a fake indexedDB since test are running in node environment.
        const db = new Dexie("MPADMIN", {indexedDB: IndexedDB, IDBKeyRange: IDBKeyRange}),
            lightLayerList = [
                {
                    id: "100",
                    name: "testWMS100",
                    selected: 0,
                    visibility: 1
                },
                {
                    id: "101",
                    name: "testWMS101",
                    selected: 1,
                    visibility: 1
                },
                {
                    id: "102",
                    name: "testWMS102",
                    selected: 1,
                    visibility: 0
                }
            ],
            rawLayerList = [
                {
                    "id": "100",
                    "name": "testWMS100",
                    "url": "dummy100.example.com",
                    "typ": "WMS"
                },
                {
                    "id": "101",
                    "name": "testWMS101",
                    "url": "dummy101.example.com",
                    "typ": "WMS"
                },
                {
                    "id": "102",
                    "name": "testWMS102",
                    "url": "dummy102.example.com",
                    "typ": "WMS"
                }
            ];

        db.version(2).stores({
            rawLayerList: "&id, typ, source",
            lightLayerList: "&id, name, source"
        });

        db.lightLayerList.bulkPut(lightLayerList).catch(Dexie.BulkError, (err) => {
            console.error("error:", err);
        });

        db.rawLayerList.bulkPut(rawLayerList).catch(Dexie.BulkError, (err) => {
            console.error("error:", err);
        });

        // Create vuex store containing db handle. Henceforth it's not allowed to change db directly (without mutation).
        store = new Vuex.Store({
            state: {
                graph: {
                    nodes: {}
                },
                portalconfig: {
                    portalTitle: {
                        title: "dummyPortal"
                    }
                },
                themenconfig: {
                    Fachdaten: {
                        Layer: [
                            {id: "101"},
                            {id: "102"}
                        ]
                    }
                },
                styleConf: [{styleId: "style1", rules: []}, {styleId: "style2", rules: []}]
            },
            getters: {
                chapters: () => [],
                graph: state => state.graph,
                portalconfig: state => state.portalconfig,
                themenconfig: state => state.themenconfig,
                styleConf: state => state.styleConf,
                version: getVersion,
                hostedMasterportalURL: () => "dummyHostedMasterportalURL",
                language: () => "test",
                hostedMasterportalVersion: getHostedMasterportalVersion,
                loadedLayerConf: () => "",
                loadedRestConf: () => "",
                loadedStyleConf: () => ""
            }
        });

        dbContainer.db = db;

    });

    afterEach(() => {
        // Tidy up.
        jest.resetAllMocks();
        jest.restoreAllMocks();

        return dbContainer.db.lightLayerList.clear().then(() => {
            dbContainer.db.close();
            dbContainer.db = null;
        });
    });


    it("Should be a vue instance", () => {
        const wrapper = shallowMount(PreviewAndPublish, {store, localVue});
        expect(wrapper.vm).toBeTruthy();
    });

    describe("Should derive a proper name for downloaded files", () => {

        let wrapper;

        beforeAll(() => {
            wrapper = shallowMount(PreviewAndPublish, {store, localVue});
        });

        beforeEach(() => {
            jest.useFakeTimers();
        });

        afterEach(() => {
            jest.useRealTimers();
        });

        it("GeoViewer: Portal - Basic @ 1. Mai 2018 11:42 Uhr", () => {
            jest.setSystemTime(new Date(2018, 4, 1, 11, 42));
            expect(wrapper.vm.buildFileName("masterportal", "GeoViewer: Portal - Basic", "zip")).toEqual("masterportal-GeoViewerPortalBasic-201805011142.zip");
        });

        it("Undefined title @ 1. Mai 2018 11:42 Uhr", () => {
            jest.setSystemTime(new Date(2018, 4, 1, 11, 42));
            // eslint-disable-next-line no-undefined
            expect(wrapper.vm.buildFileName("masterportal", undefined, "zip")).toEqual("masterportal-201805011142.zip");
        });

        it("Temperatur 100m unter der Erdoberfläche @ 12. Dezember 1999 00:01 Uhr", () => {
            jest.setSystemTime(new Date(1999, 11, 12, 0, 1));
            expect(wrapper.vm.buildFileName("masterportal", "Temperatur 100m unter der Erdoberfläche", "zip")).toEqual("masterportal-Temperatur100mUnterDerErdoberflaeche-199912120001.zip");
        });

    });

    describe("Should offer configured masterportal instance for downloading as a zip archive", () => {
        it("Should trigger download by click", () => {
            const stubOnDownloadFull = jest.spyOn(PreviewAndPublish.methods, "onDownloadFull"),
                wrapper = mount(PreviewAndPublish, {store, localVue, vuetify}),
                downloadFullHeader = wrapper.find(".downloadFullHeader");

            stubOnDownloadFull.mockResolvedValue();

            // Open accordion tab
            downloadFullHeader.trigger("click");

            wrapper.find(".downloadFullContent button").trigger("click");

            expect(stubOnDownloadFull.mock.calls.length).toBe(1);
        });

        it("Should offer zip archive containing configured instance", () => {

            expect.assertions(8);

            // mock axios.get to return local dummy zip file
            axios.get.mockReturnValueOnce(new Promise((resolve) => {

                const zip = new JSZip();
                zip.file("dummyFile", "dummyContent");

                zip.generateAsync({type: "blob"}).then((zipFile) => {
                    resolve({
                        data: zipFile
                    });
                });
            }));

            const wrapper = shallowMount(PreviewAndPublish, {store, localVue}),
                stubBuildConfigJSON = jest.spyOn(wrapper.vm, "buildConfigJSON"),
                stubBuildServicesInternetJSON = jest.spyOn(wrapper.vm, "buildServicesInternetJSON"),
                stubBuildFileName = jest.spyOn(wrapper.vm, "buildFileName");

            stubBuildConfigJSON.mockResolvedValue({
                Portalconfig: {
                    portalTitle: {
                        title: "dummyPortal"
                    }
                },
                Themenconfig: {
                    Layer: [
                        {
                            "id": "dummyLayer"
                        }
                    ]
                }
            });

            stubBuildServicesInternetJSON.mockResolvedValue([
                {
                    "id": "42",
                    "name": "dummyLayer",
                    "url": "dummy.example.com",
                    "typ": "WMS"
                }
            ]);

            stubBuildFileName.mockResolvedValue("dummyFileName");

            return wrapper.vm.onDownloadFull().then(() => {

                expect(saveAs.mock.calls.length).toBe(1);

                const [receivedZipFile, receivedFileName] = saveAs.mock.calls[0];

                expect(receivedFileName).toEqual("dummyFileName");

                return JSZip.loadAsync(receivedZipFile);
            }).then(zip => {

                const filePromise = zip.file("dummyFile").async("string"),
                    configJSONPromise = zip.folder("Basic").file("config.json").async("string"),
                    servicesIntenterJSONPromise = zip.folder("Basic").folder("resources").file("services-internet.json").async("string");

                return Promise.all([
                    filePromise,
                    configJSONPromise,
                    servicesIntenterJSONPromise
                ]);
            }).then(([
                file,
                configJSON,
                servicesInterntJSON
            ]) => {

                const parsedConfigJSON = JSON.parse(configJSON),
                    parsedServicesInternetJSON = JSON.parse(servicesInterntJSON);


                expect(file).toEqual("dummyContent");

                expect(parsedConfigJSON.Portalconfig.portalTitle.title).toEqual("dummyPortal");
                expect(parsedConfigJSON.Themenconfig.Layer.length).toEqual(1);
                expect(parsedConfigJSON.Themenconfig.Layer[0].id).toEqual("dummyLayer");

                expect(parsedServicesInternetJSON.length).toEqual(1);
                expect(parsedServicesInternetJSON[0]).toEqual({
                    "id": "42",
                    "name": "dummyLayer",
                    "url": "dummy.example.com",
                    "typ": "WMS"
                });


            });
        });
    });

    it("Should build a valid services-internet.json", () => {

        const wrapper = shallowMount(PreviewAndPublish, {store, localVue});

        return wrapper.vm.buildServicesInternetJSON().then((rawLayerList) => {

            expect(rawLayerList.length).toEqual(3);
            expect(rawLayerList).toSortedEqual([
                {
                    "id": "100",
                    "name": "testWMS100",
                    "url": "dummy100.example.com",
                    "typ": "WMS"
                },
                {
                    "id": "101",
                    "name": "testWMS101",
                    "url": "dummy101.example.com",
                    "typ": "WMS"
                },
                {
                    "id": "102",
                    "name": "testWMS102",
                    "url": "dummy102.example.com",
                    "typ": "WMS"
                }
            ]);
        });
    });

    it("Should build a valid config.json", () => {
        const wrapper = shallowMount(PreviewAndPublish, {store, localVue});

        return wrapper.vm.buildConfigJSON().then((config) => {
            expect(config.version).toEqual("dummyVersion");
            expect(config.Portalconfig.portalTitle.title).toEqual("dummyPortal");
            expect(config.Themenconfig.Fachdaten.Layer.length).toEqual(2);
        });
    });

    it("Should return url of preview instance", () => {

        expect.assertions(4);

        const getAdminBackendURL = jest.spyOn(PreviewAndPublish.computed, "adminBackendURL");
        getAdminBackendURL.mockReturnValue("dummyAdminBackendURL");

        // eslint-disable-next-line one-var
        const wrapper = shallowMount(PreviewAndPublish, {store, localVue}),
            stubBuildConfigJSON = jest.spyOn(wrapper.vm, "buildConfigJSON"),
            stubBuildServicesInternetJSON = jest.spyOn(wrapper.vm, "buildServicesInternetJSON");


        axios.post.mockReturnValueOnce(new Promise((resolve) => {
            resolve({
                data: "dummyID"
            });
        }));

        stubBuildConfigJSON.mockResolvedValue({
            "blåbær": true
        });

        stubBuildServicesInternetJSON.mockResolvedValue([]);

        return wrapper.vm.onPreview().then(() => {
            expect(axios.post.mock.calls.length).toEqual(1);
            expect(axios.post.mock.calls[0][0]).toEqual("dummyAdminBackendURL/configs/");
            expect(axios.post.mock.calls[0][1]).toEqual({
                "blåbær": true
            });

            expect(wrapper.vm.previewURL).toEqual("dummyHostedMasterportalURL/?config=dummyAdminBackendURL/configs/dummyID.json");
        });
    });

    describe("Should detect if target version and hosted masterportal version are incompatible", () => {
        it("Versions should be threated incompatible if version of hosted masterportal is unknown", () => {
            getVersion.mockReturnValue("v2.16.0");
            // eslint-disable-next-line no-undefined
            getHostedMasterportalVersion.mockReturnValue(undefined);

            const wrapper = shallowMount(PreviewAndPublish, {store, localVue});
            expect(wrapper.vm.isVersionIncompatible).toBeTruthy();
        });

        it("Version v2.16.0 and 2.16.0 should be threated as compatible", () => {
            getVersion.mockReturnValue("v2.16.0");
            getHostedMasterportalVersion.mockReturnValue({
                major: "2",
                minor: "16",
                patch: "0"
            });

            const wrapper = shallowMount(PreviewAndPublish, {store, localVue});
            expect(wrapper.vm.isVersionIncompatible).toBeFalsy();
        });

        it("Version 2.16.8 and 2.16.0 should be threated as compatible", () => {
            getVersion.mockReturnValue("2.16.8");
            getHostedMasterportalVersion.mockReturnValue({
                major: "2",
                minor: "16",
                patch: "0"
            });

            const wrapper = shallowMount(PreviewAndPublish, {store, localVue});
            expect(wrapper.vm.isVersionIncompatible).toBeFalsy();
        });

        it("Version 3.0.0 and 2.0.0 should be threated as incompatible", () => {
            getVersion.mockReturnValue("3.0.0");
            getHostedMasterportalVersion.mockReturnValue({
                major: "2",
                minor: "0",
                patch: "0"
            });

            const wrapper = shallowMount(PreviewAndPublish, {store, localVue});
            expect(wrapper.vm.isVersionIncompatible).toBeTruthy();
        });

        it("Version 2.15.0 and 2.16.0 should be threated as incompatible", () => {
            getVersion.mockReturnValue("2.15.0");
            getHostedMasterportalVersion.mockReturnValue({
                major: "2",
                minor: "16",
                patch: "0"
            });

            const wrapper = shallowMount(PreviewAndPublish, {store, localVue});
            expect(wrapper.vm.isVersionIncompatible).toBeTruthy();
        });
    });

    /*

    it("Should return url of permanent config and permanent view instance", () => {

        expect.assertions(5);

        sinonSandbox.stub(PreviewAndPublish.computed, "adminBackendURL").returns("dummyAdminBackendURL");

        const wrapper = shallowMount(PreviewAndPublish, {store, localVue}),
            stubBuildConfigJSON = sinonSandbox.stub(wrapper.vm, "buildConfigJSON"),
            stubBuildServicesInternetJSON = sinonSandbox.stub(wrapper.vm, "buildServicesInternetJSON");

        axios.post.mockImplementationOnce((url) => {
            return new Promise((resolve) => {
                resolve({
                    data: url.split("/").pop()
                });
            });
        });

        stubBuildConfigJSON.resolves({
            "blåbær": true
        });

        stubBuildServicesInternetJSON.resolves([]);

        wrapper.vm.configName = "dummyConfigName";

        return wrapper.vm.onPublish().then(() => {
            expect(axios.post.mock.calls.length).toEqual(1);
            expect(axios.post.mock.calls[0][0]).toEqual("dummyAdminBackendURL/configs/dummyConfigName");
            expect(axios.post.mock.calls[0][1]).toEqual({
                "blåbær": true
            });

            expect(wrapper.vm.permanentConfigURL).toEqual("dummyAdminBackendURL/configs/dummyConfigName.json");
            expect(wrapper.vm.permanentConfigViewURL).toEqual("dummyHostedMasterportalURL/?config=dummyAdminBackendURL/configs/dummyConfigName.json");
        });
    });

    describe("Abort publish if provided configName is invalid", () => {

        it("Should abort publish when configName contains a non alphanumeric character", () => {

            expect.assertions(1);

            const wrapper = shallowMount(PreviewAndPublish, {store, localVue});
            wrapper.vm.configName = "invalidConfigName!";

            return wrapper.vm.onPublish().catch(reason => {
                expect(reason).toEqual("Invalid name");
            });

        });

        it("Should abort publish when configName is empty", () => {

            expect.assertions(1);

            const wrapper = mount(PreviewAndPublish, {store, localVue});
            wrapper.vm.configName = "";

            return wrapper.vm.onPublish().catch(reason => {
                expect(reason).toEqual("Invalid name");
            });
        });

        it("Should abort publish when configName is undefined", () => {

            expect.assertions(1);

            const wrapper = shallowMount(PreviewAndPublish, {store, localVue});

            / * eslint-disable-next-line no-undefined * /
            wrapper.vm.configName = undefined;

            return wrapper.vm.onPublish().catch(reason => {
                expect(reason).toEqual("Invalid name");
            });
        });

    });
*/
