import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { ref } from 'vue';
import { render } from '@/test/components/helper';

import LoadJSONFile from '@/pages/LoadJSONFile.vue';
import { useConfigStore } from '@/store/config';
import { useLayersStore } from '@/store/layers';

describe('LoadJSONFile', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		useConfigStore.mockReturnValue({
			admintoolConfig: {
				styleConf: 'DEFAULTSTYLE',
			},
			styleConfUrl: 'STYLISH',
			styleConf: [],
			isExcludedFeature: () => false,
		});
		useLayersStore.mockReturnValue({
			loadLayers: vi.fn(),
		});
		ctx.title = ref('TEST');
		ctx.render = render(LoadJSONFile, {
			props: {
				path: 'styleConf',
			},
			provide: {
				title: ctx.title,
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('renders title', ({ title }) => {
		expect(title.value).toBe('chapters.loading.jsons.styleConf.title');
	});
});
