import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { ref } from 'vue';
import { render } from '@/test/components/helper';

import EditConfig from '@/pages/EditConfig.vue';
import { useGraphStore } from '@/store/graph';

// Needed for mocking out OL library
vi.mock('@/lib/showMap', () => ({}));
vi.mock('@/components/GeneratedForm/InputExtent.vue', () => ({}));
vi.mock('@/components/GeneratedForm/InputCoordinate.vue', () => ({}));

describe('EditConfig', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		useGraphStore.mockReturnValue({
			graph: { nodes: { TEST: { label: 'TITLE' } } },
		});
		ctx.title = ref('TEST');
		ctx.render = render(EditConfig, {
			props: {
				path: 'TEST',
			},
			provide: {
				title: ctx.title,
			},
			stubs: {
				GeneratedForm: {
					props: [ 'node', 'configPath' ],
					template: 'GENERATEDFORM',
				},
				QuickPreview: true,
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('renders title', ({ title }) => {
		expect(title.value).toBe('TITLE');
	});
});
