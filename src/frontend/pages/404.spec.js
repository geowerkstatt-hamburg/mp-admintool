import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { fireEvent } from '@testing-library/vue';
import { render } from '@/test/components/helper';

import P404 from '@/pages/404.vue';

import { useRouter } from 'vue-router';

describe('404', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		ctx.render = render(P404);
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('initializes correctly', ({ render: { getByText } }) => {
		fireEvent.click(getByText('{$t(errors.notFound.action)}'));
		expect(useRouter().push).toHaveBeenCalled();
	});
});
