import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { render } from '@/test/components/helper';

import SelectLayers from '@/pages/SelectLayers.vue';

import { useThemenconfigStore } from '@/store/themenconfig';

describe('SelectLayers', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		useThemenconfigStore.mockReturnValue({
			treeType: 'custom',
		});
		ctx.render = render(SelectLayers, {
			stubs: {
				LightTree: true,
				CustomTree: true,
				QuickPreview: true,
			},
		});
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
});
