import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { render } from '@/test/components/helper';

import LoadConfig from '@/pages/LoadConfig.vue';

vi.mock('@/lib/devModeFlag', () => ({
	useLocalConfig: true,
}));

describe('LoadConfig', () => {
	beforeEach(ctx => {
		document.body.innerHTML = '';
		ctx.render = render(LoadConfig);
	});
	afterEach(() => {
		vi.restoreAllMocks();
	});
	it('matches snapshot', ({ render: { container } }) => {
		expect(container).toMatchSnapshot();
	});
	it('detects local config', ({ render: { getByText } }) => {
		getByText('{$t(chapters.loading.localConfig)}');
	});
});
