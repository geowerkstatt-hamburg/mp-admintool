import config from '../../app/config.admintool.json' assert { type: 'json' };
import express from 'express';
import { fileURLToPath } from 'node:url';
import getMasterportalBundle from './masterportalBundle.js';
import { getConfig, addConfig } from './configs.js';
import { cleanByAge, cleanByID } from './clean.js';

const router = express.Router();

router.get('/app/config.admintool.json', (req, res) => res.sendFile(fileURLToPath(new URL('../../app/config.admintool.json', import.meta.url))));
router.get('/app/config.masterportalApi.json', (req, res) => res.sendFile(fileURLToPath(new URL('../../app/config.masterportalApi.json', import.meta.url))));

router.get('/masterportalBundle/:version', getMasterportalBundle);

// get a stored config (preview or permanent mode)
router.get('/configs/:id', getConfig);
// add a config for preview (id is omitted) or permanent mode
router.post('/configs/:id?', addConfig);

// remove configs. clean by age is supposed to be called by cron
router.get('/clean/:id', cleanByID);
router.get('/clean', cleanByAge);

// serve masterportal config template, if filename was provided. To detect whether a
// filename or an absolute URL is provided the existence of "/" is checked.
if (typeof config.masterportalConfigJsonTemplateURL === 'string' && config.masterportalConfigJsonTemplateURL.indexOf('/') === -1) {
	router.get(`/app/${config.masterportalConfigJsonTemplateURL}`, (req, res) => res.sendFile(fileURLToPath(new URL(config.masterportalConfigJsonTemplateURL, new URL('../../app/', import.meta.url)))));
}

export default router;
