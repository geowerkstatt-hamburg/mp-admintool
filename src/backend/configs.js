import { openDb, runQuery, allQuery } from './db.js';
import md5 from 'crypto-js/md5.js';

export async function addConfig (req, res) {
	try {
		const db = await openDb();
		const date = new Date();

		// TODO: verify JSON
		const jsonString = JSON.stringify(req.body);
		let mode;
		let id;

		// If no id is provided: Mark config for preview-mode and generate a unique id for each config file.
		// Its not enough to generate id by content since the time is important as well.
		if (req.params.id) {
			mode = 0;
			({ id } = req.params);
		} else {
			mode = 1;
			id = md5(jsonString + date.getTime().toString()).toString();
		}

		await runQuery(db, `${req.query.overwrite ? 'REPLACE' : 'INSERT'} INTO configs (id, config, time, mode) VALUES (?, ?, ?, ?)`, [
			id,
			jsonString,
			date.getTime(),
			mode,
		]);
		db.close();
		res.send(id);
	} catch (err) {
		if (err.errno === 19) {
			// TODO: Replace sqlite3 by better-sqlite3 which supports extended error codes
			res.status(400);
			res.send('errors.nameDuplicate');
		} else {
			res.status(500);
			res.send('errors.generalSavingError');
		}
		console.error(err);
	}
}

export async function getConfig (req, res) {
	// masterportal requires configuration-files to have the suffix json. since suffix is not needed here remove it if provided.
	const id = req.params.id.replace(/\.json$/, '');
	try {
		const db = await openDb();
		const rows = await allQuery(db, 'SELECT config FROM configs WHERE id = ?', [ id ]);
		if (rows.length !== 1) {
			res.status(400);
			res.send('Error: Invalid config ID!');
			return;
		}
		res.send(JSON.parse(rows[0].config));
		db.close();
	} catch (err) {
		console.error(err);
		res.sendStatus(500);
	}
}
