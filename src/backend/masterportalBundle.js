import { URL } from 'node:url';
import axios from 'axios';
import { HttpsProxyAgent } from 'https-proxy-agent';

export default async function getMasterportalBundle (req, res) {
	const regex = /v(\d+)\.(\d+)\.(\d+)/;
	const version = req.params.version.replace(regex, '$1.$2.$3');
	const axiosConfig = {
		responseType: 'stream',
	};
	const httpsProxyString = process.env.https_proxy;
	const url = `https://api.bitbucket.org/2.0/repositories/geowerkstatt-hamburg/masterportal/downloads/${version === 'dev' ? 'examples' : `examples-${version}`}.zip`;

	// If the environment variable https_proxy is defined it's necessary to check whether the protocol is "http" or "https". When an
	// unencrypted proxy is used tunneling is required, which is done by the HttpsProxyAgent package. It's important to disable axios
	// proxy in that case to prevent using the proxy twice. In case https_proxy in encrypted (or no proxy is required) axios sets up
	// everything on its own.
	if (httpsProxyString) {
		const httpsProxy = new URL(httpsProxyString);
		if (!httpsProxy.protocol.match(/https/)) {
			axiosConfig.httpsAgent = new HttpsProxyAgent(httpsProxyString);
			axiosConfig.proxy = false;
		}
	}

	try {
		const responseStream = await axios.get(url, axiosConfig);
		responseStream.data.pipe(res);
	} catch (err) {
		res.send(err.toString());
	}
}
