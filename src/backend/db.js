import sqlite3 from 'sqlite3';
import config from '../../app/config.admintool.json' assert { type: 'json' };

function getDbPath () {
	if (config.dbPath) {
		return config.dbPath;
	}
	return './configs.sqlite';
}

function wrap (fn) {
	return new Promise((resolve, reject) => {
		const res = fn((err, alt) => {
			if (err) {
				reject(err);
			}
			resolve(alt || res);
		});
	})
}

export async function openDb (mod = 0) {
	return await wrap(cb => new sqlite3.Database(getDbPath(), sqlite3.OPEN_READWRITE | mod, cb));
}

export async function runQuery (db, query, params) {
	return await wrap(cb => db.run(query, params, cb));
}

export async function allQuery (db, query, params) {
	return await wrap(cb => db.all(query, params, cb));
}

export async function initDb () {
	const dbPath = getDbPath();
	console.info(`Load masterportal configurations from '${dbPath}'.`);

	try {
		const db = await openDb(sqlite3.OPEN_CREATE);

		// create table if not exists:
		// id: name of file (.json is omitted). Will be generated for preview mode
		// config: the configuration
		// time: timestamp of creation. Tells when to delete the item (preview mode)
		// mode: 0 -> permanent mode; 1 -> preview mode
		await runQuery(db, `CREATE TABLE IF NOT EXISTS configs(
			id text PRIMARY KEY,
			config text NOT NULL,
			time integer NOT NULL,
			mode integer NOT NULL
		)`, []);

		db.close();
	} catch (err) {
		console.error(err);
		process.exit(1);
	}
}
