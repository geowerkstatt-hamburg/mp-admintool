import { openDb, runQuery } from './db.js';

export async function cleanByAge (req, res) {
	try {
		const db = await openDb();

		// time in hours after a config should be removed if in preview-mode
		const cleanInterval = 24;
		const limitTime = new Date().getTime() - cleanInterval * 3600 * 1000;

		await runQuery(db, 'DELETE FROM configs WHERE mode = 1 AND time <= ?', [ limitTime ]);
		db.close();
		res.sendStatus(204);
	} catch (err) {
		console.error(err);
		res.sendStatus(500);
	}
}

export async function cleanByID (req, res) {
	try {
		const db = await openDb();
		await runQuery(db, 'DELETE FROM configs WHERE id = ?', [ req.params.id ]);
		db.close();
		res.sendStatus(204);
	} catch (err) {
		console.error(err);
		res.sendStatus(500);
	}
}
