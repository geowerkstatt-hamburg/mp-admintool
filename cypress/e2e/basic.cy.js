describe('Basic navigation', () => {
	it('allows navigating', () => {
		cy.visit('https://localhost:9002');
		cy.contains('Willkommen bei Masterportal Admin');
		cy.contains('Ausgangskonfiguration').click();
		cy.url().should('include', 'loadConfig');
	});
});
