# Masterportal Admin

> *An English version of this README is available. [Switch to English version](README.md)*

Diese Anwendung unterstützt die Erstellung von [Masterportal](https://www.masterportal.org/)-Konfigurationsdateien.

Sie bietet einen geführten Prozess, welcher die\*den Nutzer\*in durch verschiedene Formulare zur Konfiguration führt.
Nach der Eingabe aller Informationen, Beispielansichten können zur Überprüfung der Ergebnisse genutzt werden, und die\*der Nutzer\*in kann ihr\*sein Portal veröffentlichen.

Diese Dokumentation besteht aus den folgenden Abschnitten:

- [Konfiguration](#markdown-header-konfiguration)
- [Installation](#markdown-header-installation)
- [Troubleshooting / Bekannte Probleme](#markdown-header-troubleshooting)

If you are a developer and interested in contributing to *Masterportal Admin*, please also have a look at the [developer's corner](doc/dev-corner.md).
This page is available in English only.

## Konfiguration

Die *Masterportal Admin*-Konfiguration besteht aus zwei Dateien:

- [config.admintool.json](#markdown-header-configadmintooljson):
  Diese Datei enthält die grundlegenden Konfigurationselemente, insbesondere die URLs der 3J-Konfigurationsdateien (`services.json`, `rest-services.json` und `style.json`) welche genutzt werden.
- [config.masterportalApi.json](#markdown-header-configmasterportalapijson):
  Diese Datei enthält die masterportalAPI-Konfiguration für Vorschau und Auswahl *innerhalb von Masterportal Admin*.
  Die Konfiguration hat keinen Einfluss auf die Konfiguration der resultierenden Masterportal-Instanzen.

Die Konfiguration wird innerhalb des Ordners `app` abgelegt.

### config.admintool.json

Die Konfigurationsdatei enthält die grundlegenden Konfigurationselemente.

Die Bedeutung der Konfigurationselemente in der `config.admintool.json` wird im Folgenden beschrieben:

|Option|Beschreibung|Beispiel|
|------|------------|--------|
|`layerConf`|URL zur `services.json`. Alle Dienste dort werden in *Masterportal Admin* verfügbar sein. Die Datei `config.masterportalApi.json` (siehe unten) bezieht sich auch hierauf.|`"http://geoportal-hamburg.de/lgv-config/services-internet.json"`|
|`restConf`|URL zur `rest-services.json`. Alle REST-Dienste dort werden in *Masterportal Admin* verfügbar sein.|`"http://geoportal-hamburg.de/lgv-config/rest-services-internet.json"`|
|`styleConf`|URL zur `style.json`. Alle Styles dort werden in *Masterportal Admin* verfügbar sein.|`"http://geoportal-hamburg.de/lgv-config/style_v3.json"`|
|`hostedMasterportalURL`|Ein separat gehostetes *Masterportal* ist notwendig zur Vorschau und Veröffentlichung von Konfigurationen. Die URL dazu ist hier anzugeben.|`"https://141.91.163.211/stable/admin"`|
|`hostedMasterportalURLRouted`|Die URL eines *Masterportal*s mit aktiviertem `mod-rewrite`. Damit können mehr Pfadfeatures genutzt werden. Standardmäßig `false`, was anzeigt, dass `hostedMasterportalURL` genutzt werden soll.|
|`basePath`|Zusätzlicher Pfadname. Zu verwenden, wenn *Masterportal Admin* nicht auf der Domain-Root gehostet wird. Beispiel: Die Verwendung von `/masterportal-admin` bedeutet, dass *Masterportal Admin* unter `http://localhost:9002/masterportal-admin/` gehostet wird. Wenn dieses Feld nicht gesetzt wird, nimmt die Applikation an, dass sie auf dem Root-Pfad läuft, also etwa `http://localhost:9002/`. Falls die Verschachtelung mehrere Ebenen tief ist, können die Teile auch als Array angegeben werden.|`["nested", "tool"]` oder "/nested/tool"|
|`masterportalConfigJsonTemplateURL`|Definition einer Vorlage für eine Masterportal-Konfiguration. Eine Vorlage kann im `app`-Verzeichnis abgelegt werden und wird dann durch das Admintool gehostet. In dem Fall muss hier als Parameter nur der Dateiname angegeben werden. Es kann auch eine Vorlage referenziert werden, welche auf einem anderen Server gehostet wird, wenn die vollständige URL angegeben wird. Es werden keine relativen URL´s unterstützt.|`masterportalConfig.json` oder `https://example.com/configTemplate.json`|
|`https`|Wenn `true` werden alle Inhalte mittels https ausgeliefert|`true`|
|`certificate`|Pfad zum Zertifikat, welches zum Auslifern mittels https verwendet werden soll. Wenn nicht angegeben, so wird ein temporäres Zertifikat generiert und verwendet.|`"./certificate.crt"`|
|`privateKey`|Pfad zum privaten Schlüssel für das angegebene Zertifikat. Wenn nicht gesetzt, so wird ein temporäres Zertifikat generiert und verwendet.|`"./privateKey.key"`|
|`dbPath`|Pfad zur SQLite-Datenbank, welche zum Ablegen der Masterportal-Konfigurationen verwendet wird.|`"./configs.sqlite"`|
|`expertMode`|Konfiguriert, ob die Expertenmodus-Checkbox angezeigt wird.|`false`|
|`excludedChapters`|Hier werden Kapitel gesetzt, die vom Admin-Tool ingoriert werden sollen um z.B. die Komplexität der Konfiguration zu reduzieren. Erlaubte Werte sind in untenstehender Tabelle aufgelistet.|`["portalconfig"]`|
|`excludedFeatures`|Hier werden Tools und Features gesetzt, die vom Admin-Tool ignoriert werden sollen um z.B. die Komplexität der Konfiguration zu reduzieren. Erlaubte Werte sind in untenstehender Tabelle aufgelistet.|`["wfsSearch"]`|
|`renameMatrix`|In dieser Matrix werden die Beschriftungen der Nodes und Edges neu gesetzt. Es kann so Beispielsweise der Name das Nodes `Themenconfig` in der GUI zu "Themenauswahl und Karteninhalte" umgeschrieben werden.|`{"DE": {"Themenconfig": "Themenauswahl und Karteninhalte"}}`|
|`renameKeys`|In dieser Matrix werden die Beschriftungen der Nodes und Edges neu gesetzt. Es kann so beispielsweise der Name des Nodes unter dem Pfad `Portalconfig.menu.tools.children` in der GUI zu "Tools" umgeschrieben werden.|`{"DE": {"Portalconfig.menu.tools.children": "Tools"}}`|
|`sortAlphabetically`|Hier werden Nodes gesetzt, deren Edges in der GUI alphabetisch sortiert werden sollen, anstatt die Reihenfolge im Code zu übernehmen. So lassen sich beispielsweise die verschiedenen Controls in `Portalconfig.controls` alphabetisch sortieren.|`["Portalconfig.controls"]`|
|`isSortable`|Hier werden Nodes gesetzt, deren Edges in der GUI umsortiert werden können. So lassen sich beispielsweise die verschiedenen Tools in `Portalconfig.menu.tools.children` für das Menü sortieren.|`["Portalconfig.menu.tools.children"]`|

Folgende Werte können in `excludedFeatures` verwendet werden:

| value | description |
| --- | ----------- |
|`"loadconfigfromfile"`| Bereich "Hochladen oder aus dem Internet laden" in Kapitel "Ausgangskonfiguration wählen".|
|`"loadconfigfromtemplate"`| Bereich "Eine Vorlage als Ausgangspunkt verwenden" in Kapitel "Ausgangskonfiguration wählen".|
|`"loadconfigfromversion"`| Bereich "Default einer Masterportalversion laden" in Kapitel "Ausgangskonfiguration wählen".|
|`"loadjsonconfigfromurl"`| Bereich "Konfiguration aus dem Internet laden" in Kapiteln "Kartendienste", "REST-Dienste" und "Styles".|
|`"loadjsonconfigfromfile"`| Bereich "Lokale Konfigurationsdatei hochladen" in Kapiteln "Kartendienste", "REST-Dienste" und "Styles".|
|`"exportpreview"`| Bereich "Vorschau" in Kapitel "Export".|
|`"exportpublish"`| Bereich "Veröffentlichung" in Kapitel "Export".|
|`"exportdownloadfull"`| Bereich "Download einer konfigurierten Masterportal-Instanz" in Kapitel "Export".|
|`"exportdownloadjson"`| Bereich "Download der zentralen Konfigurationsdatei" in Kapitel "Export".|
|`tool`| Name des Tools innerhalb eines Unterkapitels in "Portaleinstellungen". Z.B. `"wfsSearch"`.|

Folgende Werte können in `excludedChapters` verwendet werden:

| Wert | Typ | Beschreibung |
| ---- | --- | ------------ |
|`"introduction"`| Hauptkapitel | Kapitel "Globale Konfiguration wählen" |
|`"loadconfig"`| Hauptkapitel | Kapitel "Ausgangskonfiguration wählen" |
|`"portalconfig"`| Hauptkapitel | Kapitel "Portaleinstellungen" |
|`"themenconfig"`| Hauptkapitel | Kapitel "Themenauswahl und Karteninhalte" |
|`"export"`| Hauptkapitel | Kapitel "Export" |
|`"layerconf"`| Unterkapitel | Kapitel "Globale Konfiguration wählen" > "Kartendienste" |
|`"restconf"`| Unterkapitel | Kapitel "Globale Konfiguration wählen" > "REST-Dienste" |
|`"styleconf"`| Unterkapitel | Kapitel "Globale Konfiguration wählen" > "Styles" |
|`"themenconfig.groupconfig"`| Unterkapitel | Kapitel "Themenauswahl und Karteninhalte" > "Groupconfig" |
|`"themenconfig.layerconfig"`| Unterkapitel | Kapitel "Themenauswahl und Karteninhalte" > "Layerconfig" |
|`"themenconfig.styleconfig"`| Unterkapitel | Kapitel "Themenauswahl und Karteninhalte" > "Styleconfig" |
|`"themenconfig.gficonfig"`| Unterkapitel | Kapitel "Themenauswahl und Karteninhalte" > "Gficonfig" |
|`"portalconfig." + x`| Unterkapitel | Kapitel "Portaleinstellungen" > X. Wobei X durch das entsprechende Unterkapitel ersetzt werden muss. Z.B. `"portalconfig.menu"`. Siehe die URL-Leiste für weitere Werte. |

### config.masterportalApi.json

Diese Konfigurationsdatei kann auch nach dem Build angepasst werden.
Diese Konfiguration bezieht sich *nicht* auf das *Masterportal*, sondern auf die Karte, die genutzt wird, um die Layervorschau und die Auswahl von Punkten und Ausdehnungen zu betreiben.
Hierfür wird intern die [masterportalAPI](https://bitbucket.org/geowerkstatt-hamburg/masterportalapi/src/master/) genutzt.

Üblicherweise wird es reichen hier `"startCenter"` und `"extent"` anzupassen, um sie auf Werte zu bringen, die sich mit dem Einsatzgebiet von *Masterportal Admin* decken.
Standardmäßig sind hier Werte für Hamburg eingetragen.

Außerdem kann hier unter `"layers"` angegeben werden, welche Hintergrundkarte angezeigt werden soll.
Diese ist *nicht* für das konfigurierte Masterportal relevant, sondern dient dem Nutzer nur zur Orientierung.

## Installation

Es gibt zwei Optionen, *Masterportal Admin* zu betreiben:

1. [Lokale Installation](#markdown-header-lokale-installation)
1. [Docker-Umgebung](#markdown-header-docker-umgebung)

### Lokale Installation

#### Voraussetzungen

Um *Masterportal Admin* lokal zu betreiben, sind folgende Komponenten notwendig:

- [Node](https://nodejs.org) >= 8.12.0 (notwendig zum Bauen der Anwendung und Betreiben des Backends)
- *Masterportal Admin*.
  Der Quellcode kann über Git mit folgendem Kommando bezogen werden: `git clone --single-branch --branch master https://bitbucket.org/geowerkstatt-hamburg/mp-admintool.git`

Es ist empfohlen, eine Instanz des Masterportals zu installieren.
Die Installation wird in der [Installationsanleitung des Masterportals](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/dev/doc/setup.de.md) beschrieben.
Als Alternative kann eine existierende Instanz mit denselben 3J-Konfigurationsdateien genutzt werden (`services.json`, `rest-services.json` und `style.json`).

#### Bau

Das Frontend muss gebaut werden, bevor *Masterportal Admin* ausgeführt werden kann.
Der Bauprozess kann auf einer anderen Maschine durchgeführt werden und erfordert mehr Abhängigkeiten.

```bash
NODE_ENVIRONMENT=development npm i
NODE_ENVIRONMENT=production npm run build
```

Ein Neubau des Frontends ist notwendig bei Änderungen des Quellcodes oder bei Anpassungen der `basePath`-[Konfiguration](#markdown-header-konfiguration)soption in der `config.admintool.json`.

#### Betrieb

Wenn das Frontend auf einer anderen Maschine gebaut wurde, müssen nun die Laufzeitabhängigkeiten installiert werden:

```bash
NODE_ENVIRONMENT=production npm i
```

Ansonsten können jetzt die Bauzeitabhängigkeiten entfernt werden:

```bash
NODE_ENVIRONMENT=production npm prune
```

*Masterportal Admin* kann dann mit dem folgenden Kommando gestartet werden:

```bash
NODE_ENVIRONMENT=production npm run prod
```

Wir empfehlen, einen Prozessmanager wie PM2 für ein stabiles System, welches *Masterportal Admin* betreibt, zu nutzen, zum Beispiel zwischen Neustarts.

Beim Aufruf des Endpunkts `/clean` werden alle Vorschaukonfigurationen gelöscht, welche älter als 24 Stunden sind.
Siehe auch die Beschreibung der Export-Seite von *Masterportal Admin*.
Wenn dieses Aufräumen regelmäßig passieren soll, empfehlen wir, einen Cronjob diese URL aufrufen zu lassen, in einem Zeitintervall Ihrer Wahl.

### Docker-Umgebung

Das Git-Repository stellt ein `Dockerfile` und eine `docker-compose.yaml` bereit.
Es kann entweder [Docker direkt](#markdown-header-docker-nutzen) verwendet werden, um *Masterportal Admin* zu betreiben, oder es kann [Docker Compose](#markdown-header-docker-compose) genutzt werden.
Natürlich muss Docker (und, wenn Docker Compose genutzt wird, das natürlich auch) installiert werden um den weiteren Anweisungen zu folgen.

In beiden Fällen muss zunächst das Git-Repository geklont werden:

```bash
git clone --single-branch --branch master https://bitbucket.org/geowerkstatt-hamburg/mp-admintool.git
```

#### Docker nutzen

Um ein Docker-Image zu bauen, muss das Image gebaut werden:

```bash
docker build --tag mpadmin .
```

Wenn der Container keinen direkten Internetzugang hat, sollten die Argumente `HTTP_PROXY` und `HTTPS_PROXY` genutzt werden:

```bash
docker build --tag mpadmin --build-arg HTTP_PROXY="http://127.0.0.1:3128" --build-arg HTTPS_PROXY="http://127.0.0.1:3128" .
```

Um einen `basePath` zu setzen, muss dieser vor dem Bauen in der `config.admintool.json` angepasst/gesetzt werden.

> 💡 Das Neubauen des Frontends ist notwendig bei Aktualisierungen des Quellcodes oder bei Änderungen der `basePath`-[Konfiguration](#markdown-header-konfiguration)soption in der `config.admintool.json`.

Um das gebaute Docker-Image auszuführen, kann `docker run` wie folgt genutzt werden:

```bash
docker run -p 9001:3000 mpadmin
```

Der Container stellt Port 3000 bereit.
Mit der Option `-p` kann dieser auf den gewünschten Port auf dem Hostsystem bereitgestellt werden.
Im obigen Beispiel wird Port 9001 auf dem Hostsystem bereitgestellt.

#### docker-compose

Proxyserver können, falls notwendig, konfiguriert werden, indem die Konfigurationsdatei mit dem Namen `.env` angepasst wird.
Eine Vorlage dafür, wie diese Konfigurationsdatei aussehen kann, kann im Repository als [.env](./.env) gefunden werden.

Um einen `basePath` zu setzen, kann dieser einfach vor dem Bauen in der `config.admintool.json` angepasst/gesetzt werden.

Um den Container zu bauen, kann das folgende Kommando genutzt werden:

```bash
docker-compose build
```

Um den Container zu starten, muss einfach der Container gestartet werden:

```bash
docker-compose up -d
```

Der (externe) Port kann auf einen Port angepasst werden, der zu den lokalen Anforderungen passt.
Das kann durch Anpassung des entsprechenden Werts in der `docker-compose.yaml` erreicht werden.
Der externe Port ist standardmäßig Port 9001.

Die Konfigurationen werden standardmäßig in `/mp-admintool/configs.sqlite` im Container gespeichert.
Dieser Pfad kann durch die Änderung der `dbPath`-Option in der [config.admintool.json](#markdown-header-configadmintooljson) angepasst werden.
Standardmäßig gehen diese Konfigurationen beim Neustart des Containers verloren.

Wenn die Konfigurationen permanent zwischen Container-Neustarts gespeichert werden sollen, kann ein Volume für diese Datei definiert werden.
Hierfür muss folgendes der `docker-compose.yaml` hinzugefügt werden:

```yaml
services:
  mpadmin:
    [...]
    volumes:
      - mpadmin_db:/mp-admintool/configs.sqlite
volumes:
  mpadmin_db:
```

#### Veraltetes Dockerfile

Bis Version 1.0.0 von *Masterportal Admin* wurde eine alte Version des Dockerfiles genutzt.
Diese Version wird als `Dockerfile.git` mitgeliefert.
Es wird allerdings empfohlen, die neuen Dateien `Dockerfile` und `docker-compose.yaml` zu nutzen, wenn es nicht einen guten Grund gibt, davon abzuweichen.

## Troubleshooting

### Vorkonfigurierte Masterportal-Bundle-ZIP-Datei wird nicht bereitgestellt

Masterportal Admin fungiert als Proxy, um das Masterportal-Bundle abzurufen, da ein direkter Zugriff nicht möglich ist (CORS).
Wenn Masterportal Admin hinter einem Proxy läuft und einen direkten Internetzugang hat, kann das bei einer fehlerhaften Proxykonfiguration fehlschlagen.
In diesem Fall überprüfen Sie bitte, ob der Proxy im lokalen Setup definiert ist.
Überprüfen Sie bitte außerdem, ob die Proxykonfiguration von Docker korrekt ist, wenn Docker für *Masterportal Admin* genutzt wird.
Das kann mittels `docker info` getan werden.
Bitte beachten Sie, dass es wichtig ist, die Protokolle zu setzen, wenn ein HTTP-Proxy für verschlüsselte Verbindungen genutzt werden soll!

### Masterportal Admin funktioniert nicht im privaten Modus

Die *IndexedDB* wird zur Speicherung von Informationen genutzt.
Einige Browser (z.B. Firefox) können den Zugang zur Datenbank verhindern, wenn sie im privaten Modus betrieben werden.
Deswegen funktioniert das Tool gegebenenfalls nicht im privaten Modus.

### Vorschaukarten funktionieren nicht

*Masterportal Admin* bietet Eingabeelemente auf Basis von [masterportalAPI](https://bitbucket.org/geowerkstatt-hamburg/masterportalapi/src/master/)-Karten an.
Wenn `layerConf` geändert wird, muss eventuell auch die Information für das Eingabeelement angepasst werden.
Dies ist in der Datei `config.masterportalApi.json` möglich, wo die ID des Hintergrundlayers, die initialen Zentrumskoordinaten, und die Ausdehnung der Karte definiert werden können.
