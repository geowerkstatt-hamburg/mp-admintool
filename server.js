/* eslint-env node */
/* eslint-disable no-sync */

// Server that delivers frontend and offers backend services.
// Features hot reloading for development mode.
// Custom parameters by index:
//      0: port

import path from 'node:path';
import { fileURLToPath } from 'node:url';
import fs from 'node:fs';
import https from 'node:https';
import http from 'node:http';
import express from 'express';
import compression from 'compression';
import pem from 'pem';

import backendRouter from './src/backend/index.js';
import { initDb } from './src/backend/db.js';

import config from './app/config.admintool.json' assert { type: 'json' }; // eslint-disable-line
import { initAd } from './app/activeDirectory/index.js';

const app = express();
const args = process.argv.slice(2);
const port = parseInt(args[0], 10) || 3000;
const devModeFlag = process.env.NODE_ENV !== 'production';
const useLocalConfig = process.env.VITE_USE_LOCAL_CONFIG_FILES;

// error handler: express assumes four-parameter function is the error handler; "next" must be there, but isn't used, hence disabling rule
app.use((err, req, res, next) => { // eslint-disable-line
	console.error(err);
	res.status(err.httpStatusCode || 500).json({ error: err.message });
});

await initAd(app);
app.use(compression());
app.use(express.json({ limit: '2mb' }));
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Content-Type');
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
	next();
});

function getBasePath () {
	let basePathString = Array.isArray(config.basePath) ? config.basePath.join('/') : (config.basePath || '');
	if (basePathString.charAt(0) !== '/' && basePathString.length) {
		basePathString = `/${basePathString}`;
	}
	if (basePathString.slice(-1) === '/') {
		basePathString = basePathString.slice(0, -1);
	}
	return basePathString;
}

const router = express.Router();
app.use(getBasePath(), router);

router.use(backendRouter);
if (devModeFlag) {
	if (useLocalConfig) {
		router.use(express.static(fileURLToPath(new URL('./dev', import.meta.url))));
	}
	const { createServer: createViteServer } = await import('vite');
	const vite = await createViteServer({
		configFile: './vite.config.js',
		server: {
			middlewareMode: true,
		},
	});
	router.use(vite.middlewares);
} else {
	router.use(express.static(fileURLToPath(new URL('./dist', import.meta.url))));
}
router.get(`/*`, (req, res) => res.sendFile(fileURLToPath(new URL('./dist/index.html', import.meta.url))));

await initDb();

if (config.https) {
	// Use a given certificate if defined, otherwise fallback to a generated certificate.
	const snakeoilDate = new Date()
	snakeoilDate.setDate(snakeoilDate.getDate() - 7)
	const httpsOptions = {}
	if (config.certificate && config.privateKey) {
		httpsOptions.key = fs.readFileSync(config.privateKey, 'utf8');
		httpsOptions.cert = fs.readFileSync(config.certificate, 'utf8');
	} else if (fs.existsSync('./snakeoil.crt') && fs.existsSync('./snakeoil.key') && fs.statSync('./snakeoil.crt').mtime > snakeoilDate) {
		console.log('No certificate or private key defined. There is a cached TLS certificate found (snakeoil.{crt,key}) which will be used. This should be used for development purposes only.');
		httpsOptions.key = fs.readFileSync('./snakeoil.key', 'utf8');
		httpsOptions.cert = fs.readFileSync('./snakeoil.crt', 'utf8');
	} else {
		console.log('No certificate or private key defined. Generate a temporary selfsinged certificate and private key. This should be used for development purposes only.');
		try {
			({ clientKey: httpsOptions.key, certificate: httpsOptions.cert } = await new Promise((resolve, reject) => {
				pem.createCertificate({ days: 14, selfSigned: true }, (err, keys) => {
					if (err) {
						reject(err);
						return;
					}
					resolve(keys);
				});
			}));
			fs.writeFileSync('./snakeoil.crt', httpsOptions.cert);
			fs.writeFileSync('./snakeoil.key', httpsOptions.key);
		} catch (err) {
			console.error(err);
			process.exit(1);
		}
	}
	const httpsServer = https.createServer(httpsOptions, app);
	httpsServer.listen(port, () => console.info(`Masterportal Admin listening on port ${port} (https).`));
} else {
	const httpServer = http.createServer(app);
	httpServer.listen(port, () => console.info(`Masterportal Admin listening on port ${port} (http).`));
}
