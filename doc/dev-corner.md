# Developer's corner

This file contains additional information for developers:

- [Architecture overview](#markdown-header-architecture-overview)
- [Git Workflow](#markdown-header-git-workflow)
- [Coding conventions](#markdown-header-coding-conventions)
- [Running Masterportal Admin](#markdown-header-running-masterportal-admin)
- [Linting](#markdown-header-linting)
- [Running unit tests](#markdown-header-running-unit-tests)
- [Running end-to-end tests](#markdown-header-running-end-to-end-tests)
- [Remarks](#markdown-header-remarks)

## Architecture overview

The architecture of *Masterportal Admin* is described by the following image.
The description is only available in German at the moment.

![The image shows an overview about the different endpoints and components of frontend, backend and the Bitbucket API](components.de.png "Architecture overview")

As *Masterportal Admin* handles with quite a lot of configuration files, the following image describes the connections between them.
The description is only available in German at the moment.

Please note that `config.json` was renamed to `config.admintool.json` and `mapConfig.json` to `config.masterportalApi.json`.
Please also note that in addition to `layerConf` and `styleConf` an option `restConf` was introduced for the `rest-services.json`.

![The image shows an overview about the different configuration files involved at Masterportal Admin.](config.de.png "Overview about configuration files")

## Git Workflow

Please read the Masterportal's [Git Workflow](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/dev/doc/gitWorkflow.md) regarding best practices.
However, *Masterportal Admin* has **master** as its main branch, and all PRs should target it. Annotations regarding a *stable* branch do not apply.

A more details workflow may be provided, should the need arise.

## Release Workflow

New versions will be released as git-tags, manually. Before creating
a tag, please update the version in `package.json` to match the tag
version.

## Coding conventions

Please read the recommendations from the [VueJS guide](https://vuejs.org/guide), as these are mostly adopted for this project.
When it's not obvious whether a rule applies, please discuss the issue with the code maintainers before doing so.

Please also read the Masterportal's [Coding conventions](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/dev/doc/codingConventions.md) regarding best practices.
Many points (e.g. `A.9` and `A.1.6`) may not apply, but you'll get the overall gist of it.
Again remember: When it's not obvious whether a rule applies, please discuss the issue with the code maintainers before doing so.

More detailed conventions may be provided, should the need arise.

## Running Masterportal Admin

- Preparing your source folder and install dependencies:

```bash
npm install
```

### Development running

- Serve Masterportal Admin with hot reloading at [https://localhost:9002](https://localhost:9002).

  Since there is no certificate available at that point, a self-signed SSL certificate will be generated for 14 days lifetime.
  You are required to accept that certificate manually in your browser; you may check the fingerprint against the certificate in snakeoil.crt.

```bash
npm start          # equivalent to:
npm run dev
```

- Serve Masterportal Admin with hot reloading at [https://localhost:9003](https://localhost:9003) and overriding `config.json.md` with the file given at the [dev folder](dev).
  This is useful to test the effect of changes to a config.json before actually applying changes to Masterportal.

  Since there is no certificate available at that point, a self-signed SSL certificate will be generated for 14 days lifetime.
  You are required to accept that certificate manually in your browser; you may check the fingerprint against the certificate in snakeoil.crt.

```bash
npm run devWithLocalConfig
```

### Production running

- Build frontend files for production.
  After changing `config.admintool.json`, a rebuild is required

```bash
npm run build
```

- Build frontend files for production continuously watching for file changes.

```bash
npm run build-watch
```

- Run production bundle with express server at a given port.
  The port may be provided as an argument and otherwise defaults to 3000.

```bash
npm run prod              # Port defaults to 3000
npm run prod 8080         # Runs on port 8080
```

- Build frontend files for production and immediately after that, run the production bundle at port 3000.

```bash
npm run build-prod
```

## Linting

- Search for linting errors and warnings.

  Note that this returns failure if errors exist and success, if everything is fine or only warnings exist.
  Warnings are only shown for TODO or FIXME comments and serve as a reference where you may start optimizing or checking the code.

```bash
# checks for linting mistakes
npm run lint
```

### IDE support

The command `npm run lint` works after installation.
For IDE support, additional configuration within your IDE may be required.
Please add configuration hints for your IDE if it's not listed.
*All code contributions are required to meet the configured linting rules.*

#### Visual Studio Code

Open VSC's `settings.json` by hitting `Ctrl + ,` and opening it with the curly bracket symbol in the top right corner.

The JSON object must contain this entry so that the VSC EsLint Plugin considers Vue template files.

```json
{
	"eslint.validate": [
		"javascript", {
			"language": "vue",
			"autoFix": true
		}
	]
}
```

## Running unit tests

- Run unit tests:

```bash
npm run unit
```

- Run unit tests and update snapshots, if applicable:

```bash
npm run unit-update
```

- Run unit tests continuously:

```bash
npm run unit-watch
```

- Run unit tests with web UI:

```bash
npm run unit-ui
```

- Run unit tests with coverage analysis:

```bash
npm run coverage
```

## Running end-to-end tests

E2E tests are implemented with cypress.

- Run E2E tests locally.
  The dev server must be running in the background (`npm start`).

```bash
npm run e2e
```

- Run E2E tests locally with web UI

```bash
npm run e2e-ui
```

> 💡 If you are behind a corporate proxy, remember setting `http_proxy` and `https_proxy`.

## Remarks

### Running mpadmin behind a corporate proxy

To enable the backend to fetch the zip-file containing the *Masterportal* build, the environment-variables `http_proxy` and `https_proxy` have to bet set:

```
http_proxy = "<protocol>://<host>:<port>"
https_proxy = "<protocol>://<host>:<port>"
```

### `@masterportal/mpconfigparser`

[The package @masterportal/mpconfigparser](https://www.npmjs.com/package/@masterportal/mpconfigparser) is used in both *Masterportal* and *Masterportal Admin* to ensure the quality of the `config.json.md` files in the *Masterportal*.
Please check the NPM package description for a full-fledged definition of what the `config.json.md` is supposed to hold.
Any pushes to the *Masterportal* breaking those rules will be blocked.

From a *Masterportal Admin* perspective, unless you desire to extend the possibilities, it's enough to know that this parses a markdown file and generates the form graph from it.
