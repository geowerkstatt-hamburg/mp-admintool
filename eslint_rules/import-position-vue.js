'use strict';
/* eslint-disable max-depth */

function isPossibleDirective (node) {
	return node.type === 'ExpressionStatement'
		&& node.expression.type === 'Literal'
		&& typeof node.expression.value === 'string';
}

function isVueConfiguration (node) {
	return node.type === 'VariableDeclaration'
		&& node.kind === 'const'
		&& [ 'defineProps', 'defineEmits' ].includes(node.declarations.find(({ type }) => type === 'VariableDeclarator')?.init?.callee?.name);
}

function isVitestInSourceBlock (node) {
	return node.type === 'IfStatement'
		&& node.test.type === 'MemberExpression'
		&& node.test.property?.name === 'vitest';
}

module.exports = {
	meta: {
		type: 'suggestion',
		fixable: 'code',
		schema: [],
	},
	create (context) {
		return {
			Program: n => {
				const { body } = n;
				if (!body) return;
				const message = 'Import in body of module; reorder to top.';
				const sourceCode = context.getSourceCode();
				const originSourceCode = sourceCode.getText();
				let nonImportCount = 0;
				let anyExpressions = false;
				let lastLegalImp = null;
				const errorInfos = [];
				let shouldSort = true;
				let lastSortNodesIndex = 0;
				body.forEach((node, index) => {
					if ((!anyExpressions && isPossibleDirective(node)) || isVueConfiguration(node) || isVitestInSourceBlock(node)) return;
					anyExpressions = true;

					if (node.type === 'ImportDeclaration') {
						if (nonImportCount > 0) {
							for (const variable of context.getDeclaredVariables(node)) {
								if (!shouldSort) break;
								const { references } = variable;
								if (references.length) {
									for (const reference of references) {
										if (reference.identifier.range[0] < node.range[1]) {
											shouldSort = false;
											break;
										}
									}
								}
							}
							shouldSort && (lastSortNodesIndex = errorInfos.length);
							errorInfos.push({
								node,
								range: [ body[index - 1].range[1], node.range[1] ],
							});
						} else {
							lastLegalImp = node;
						}
					} else {
						nonImportCount++;
					}
				});
				if (!errorInfos.length) return;
				errorInfos.forEach((errorInfo, index) => {
					const { node } = errorInfo;
					const infos = {
						node,
						message,
					};
					if (index < lastSortNodesIndex) {
						infos.fix = fixer => fixer.insertTextAfter(node, '');
					} else if (index === lastSortNodesIndex) {
						const sortNodes = errorInfos.slice(0, lastSortNodesIndex + 1);
						infos.fix = function (fixer) {
							const removeFixers = sortNodes.map(_errorInfo => fixer.removeRange(_errorInfo.range));
							const range = [ 0, removeFixers[removeFixers.length - 1].range[1] ];
							let insertSourceCode = sortNodes.map(_errorInfo => {
								const nodeSourceCode = String.prototype.slice.apply(originSourceCode, _errorInfo.range);
								if ((/\S/).test(nodeSourceCode[0])) {
									return `\n${nodeSourceCode}`;
								}
								return nodeSourceCode;
							}).join('');
							let insertFixer = null;
							let replaceSourceCode = '';
							if (!lastLegalImp) {
								insertSourceCode =
										insertSourceCode.trim() + insertSourceCode.match(/^(\s+)/)[0];
							}
							insertFixer = lastLegalImp ?
								fixer.insertTextAfter(lastLegalImp, insertSourceCode) :
								fixer.insertTextBefore(body[0], insertSourceCode);
							const fixers = [ insertFixer ].concat(removeFixers);
							fixers.forEach((computedFixer, i) => {
								replaceSourceCode += (originSourceCode.slice(fixers[i - 1] ? fixers[i - 1].range[1] : 0, computedFixer.range[0]) + computedFixer.text);
							});
							return fixer.replaceTextRange(range, replaceSourceCode);
						};
					}
					context.report(infos);
				});
			},
		};
	},
};

/*
The MIT License (MIT)

Copyright (c) 2015 Ben Mosher
Copyright (c) 2022 Dataport AöR

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
