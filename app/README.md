# Hints

* Edit config.admintool.json to change paths to configuration files for front- AND backend.
* Backend is supposed to return JSON whenever possible to ease usage in frontend.
* Axios is installed, so use that for outgoing requests. Works client- and server-side.
* Frontend is single-page application, hence the delivery of index.html on what would usually be 404.
