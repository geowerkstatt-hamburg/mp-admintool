const config = {
	/** @type{boolean} whether checkUser is used by app - only works on Windows */
	ldapActive: false,

	/** @type{string[]} allowed activedirectory groups */
	allowedGroups: [],

	/** @type{object} ldap connection description */
	ldap: {
		/** @type{string} url like "ldap://example.com" */
		url: '',
		/** @type{string} base domain name, e.g. "DC=example,DC=com" */
		baseDN: '',
		/** @type{string} username to log into active directory, e.g. "ADReader@NET" */
		username: '',
		/** @type{string} password for username - make sure you do not commit it - e.g. "hunter2" */
		password: '',
	},
};

module.exports = config;
