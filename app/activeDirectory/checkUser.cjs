const ActiveDirectory = require('activedirectory');
const { ldap, allowedGroups } = require('./config');
const env = process.env.NODE_ENV ? process.env.NODE_ENV.trim() : 'dev';
const ad = new ActiveDirectory(ldap);

function checkUser (req, res, next) {
	// 5: Check if requesting User is assigned to one of the allowed groups
	function step5 (grouplist) {
		const mappenGrouplist = grouplist.map(group => group.toUpperCase());
		const matches = mappenGrouplist.filter(group => allowedGroups.indexOf(group) !== -1);

		if (matches.length !== 0) {
			console.info(`Zugriff erlaubt: ${res.locals.username} (${res.locals.accountname}) in Gruppen ${matches.join(', ')} gefunden.`);
			next();
			return;
		}

		console.info(`Keine Gruppenübereinstimmung für User ${res.locals.username} (${res.locals.accountname}) gefunden.`);
		res.send(`<p>Hallo ${res.locals.username}, sie sind leider nicht berechtigt auf diese Seiten zuzugreifen! Bitte wenden sie sich an den Administrator. </p>`);

	}

	// 4: Get groups of requesting User
	function step4 (username) {
		ad.getGroupMembershipForUser(username, (err, groupsfound) => {
			if (err) {
				console.info(`ERROR: ${JSON.stringify(err)}`);
				return;
			}

			if (!groupsfound) {
				console.info(`No Groups for ${username} found.`);
			} else {
				const grouplist = groupsfound.map(ug => ug.cn);
				step5(grouplist);
			}
		});
	}

	// 3: Find the requesting User
	function step3 () {
		const username = res.locals.accountname;
		let fullname;

		ad.findUser(username, (err, user) => {
			if (err) {
				console.info(`ERROR: ${JSON.stringify(err)}`);
				return;
			}

			if (!user) {
				console.info(`User: ${username} not found.`);
			} else {
				fullname = (`${user.givenName} ${user.sn}`);
				// Vollen Namen in Variable für den request schreiben
				res.locals.username = fullname;
				step4(username);
			}
		});
	}

	function step2 () {
		// Nutzergruppe wird nur bei Zugriff auf Anwendung geprüft, bei /api etc. wird nur der Nutzername benutzt. Performance.
		if (req.url !== '/') {
			next();
			return;
		}

		step3();
	}

	// // // "Step 1"
	// Nutze node-Modul für Windows-Authentifizierung wenn lokal/dev.
	// Auf Prod übernimmt das der apache und schreibt das in den header des Requests.
	if (env !== 'production') {
		const NodeSspi = require('node-sspi');
		const nodeSSPIObj = new NodeSspi({
			retrieveGroups: false,
			perRequestAuth: false,
		});

		nodeSSPIObj.authenticate(req, res, () => {
			if (!res.finished) {
				let username = req.connection.user;

				// splitte Username um Domain zu entfernen
				username ? [ , username ] = username.split('\\') : username;

				res.locals.accountname = username;

				step2();
			}
		});
	} else {
		let accountname = req.headers.remoteuser;

		accountname ? [ , accountname ] = accountname.split('\\') : accountname;

		res.locals.accountname = accountname;
		step2();
	}
}

module.exports = checkUser;
