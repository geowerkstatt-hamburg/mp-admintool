import adConfig from './config.cjs';

const isWindows = process.platform === 'win32';

export async function initAd (app) {
	if (adConfig.ldapActive) {
		if (isWindows) {
			// conditional import - even importing (without using) on linux systems breaks the backend
			const checkUser = await import('./checkUser'); // eslint-disable-line
			if (typeof checkUser === 'function') {
				app.use(checkUser);
			}
		} else {
			throw new Error('./app/activeDirectory/config.js indicates that AD should be used, but the machine running the backend does not support it. The Admintool crashes on purpose to avoid unauthorized usage.');
		}
	}
}
