# Frontend build dependencies (including dev dependencies)
FROM node:20-bullseye-slim AS build-frontend-environment
ARG HTTP_PROXY
ARG HTTPS_PROXY
ENV http_proxy "${HTTP_PROXY}"
ENV https_proxy "${HTTPS_PROXY}"
RUN apt update && apt install -y python3
WORKDIR /mp-admintool
COPY package*.json ./
ENV NODE_ENVIRONMENT development
RUN npm ci

# Production dependencies
FROM node:20-bullseye-slim AS production-environment
ARG HTTP_PROXY
ARG HTTPS_PROXY
ENV http_proxy "${HTTP_PROXY}"
ENV https_proxy "${HTTPS_PROXY}"
WORKDIR /mp-admintool
COPY package*.json ./
ENV NODE_ENVIRONMENT production
RUN npm ci

# Frontend build (separated for speed-up)
FROM node:20-bullseye-slim AS build-frontend
ARG HTTP_PROXY
ARG HTTPS_PROXY
ARG BASEPATH
ENV http_proxy "${HTTP_PROXY}"
ENV https_proxy "${HTTPS_PROXY}"
ENV BASEPATH "${BASEPATH}"
WORKDIR /mp-admintool
COPY . .
COPY --from=build-frontend-environment /mp-admintool/node_modules node_modules
ENV NODE_ENVIRONMENT production
RUN npm run build

# Production image (exported)
FROM node:20-bullseye-slim
ARG HTTP_PROXY
ARG HTTPS_PROXY
ENV http_proxy "${HTTP_PROXY}"
ENV https_proxy "${HTTPS_PROXY}"
WORKDIR /mp-admintool
RUN apt update && apt install -y jq moreutils openssl
COPY . .
COPY --from=production-environment /mp-admintool/node_modules node_modules
COPY --from=build-frontend /mp-admintool/dist dist

ENV NODE_ENVIRONMENT production
EXPOSE 3000
CMD [ "bash", "runWithEnvConfig.sh" ]
