import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import vuetify from 'vite-plugin-vuetify';
import { viteCommonjs } from '@originjs/vite-plugin-commonjs';
import { resolve } from 'path';
import { createRequire } from 'module';
const require = createRequire(import.meta.url);

import packageJson from './package.json';
const { version } = packageJson;

import config from './app/config.admintool.json' assert { type: 'json' };

export default defineConfig({
	root: 'src/frontend',
	base: process.env.BASEPATH && JSON.parse(process.env.BASEPATH) || config.basePath || '/',
	define: {
		__VUE_OPTIONS_API__: true,
		__VUE_PROD_DEVTOOLS__: false,
		'import.meta.vitest': 'undefined',
		MP_ADMINTOOL_VERSION: JSON.stringify(version),
	},
	plugins: [
		vue(),
		vuetify({ autoImport: true }),
		viteCommonjs(),
	],
	resolve: {
		alias: {
			'@': resolve(__dirname, 'src', 'frontend'),
			'@images': resolve(__dirname, 'src', 'frontend', 'assets', 'images'),
			'@fonts': resolve(__dirname, 'src', 'frontend', 'assets', 'fonts'),
			'~': resolve(__dirname, 'node_modules'),
			'~generic': resolve(__dirname, 'src', 'frontend', 'components', 'generic'),
			'olcs/lib/olcs': resolve(__dirname, 'node_modules', 'olcs', 'lib', 'olcs'), // mitigation for ignoring package.json exports in masterportalapi
			// },
			// fallback: {
			timers: require.resolve('timers-browserify'),
			util: require.resolve('util/'),
			buffer: require.resolve('buffer/'),
			stream: require.resolve('stream-browserify'),
		},
		conditions: process.env.VITEST ? [
			'node',
		] : [],
	},
	build: {
		outDir: resolve(__dirname, 'dist'),
		emptyOutDir: true,
		sourcemap: true,
		chunkSizeWarningLimit: 1000,
		rollupOptions: {
			output: {
				manualChunks: {
					vue: [
						'vue',
						'vuetify',
					],
					ol: [
						'ol',
					],
					masterportal: [
						'@masterportal/masterportalapi',
						'@masterportal/mpconfigparser',
					],
				},
			},
		},
	},
	server: {
		hmr: {
			protocol: 'ws',
		},
	},
	test: {
		include: [
			'**/*.spec.js',
			'lib/basePath.js',
		],
		exclude: [
		],
		environment: 'jsdom',
		setupFiles: [
			'./test/vitest-setup.js',
		],
		server: {
			deps: {
				inline: [
					'vuetify',
				],
			},
		},
	},
});
