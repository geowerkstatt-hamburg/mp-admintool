#!/bin/bash
CONFIG="./app/config.admintool.json"
for key in $(jq -r 'keys[]' $CONFIG)
do
	if [ -n "$(printenv $key)" ]
	then
		jq ".$key = $(printenv $key)" $CONFIG | sponge $CONFIG
	fi
done

npm run prod
