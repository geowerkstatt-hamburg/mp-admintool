# Masterportal Admin

> *A German version of this README is available. [Switch to German version](README.de.md)*

This application supports the creation of [Masterportal](https://www.masterportal.org/) configuration files.

It offers a guided process leading the user through various forms to configure said files.
After entering all information, example views may be used to check the results, and the user may publish his or her portal.

This documentation consists of the following sections:

- [Configuration](#markdown-header-configuration)
- [Installation](#markdown-header-installation)
- [Troubleshooting / Known limitations](#markdown-header-troubleshooting)

If you are a developer and interested in contributing to *Masterportal Admin*, please also have a look at the [developer's corner](doc/dev-corner.md).

## Configuration

The *Masterportal Admin* configuration consists of two files:

- [config.admintool.json](#markdown-header-configadmintooljson):
  This file contains the basic configuration elements, especially the URLs of the 3J configuration files (`services.json`, `rest-services.json` and `style.json`) that will be used.
- [config.masterportalApi.json](#markdown-header-configmasterportalapijson):
  This file contains the masterportalAPI configuration for previews and selections *within Masterportal Admin*.
  The configuration has no effect on the configurations for resulting Masterportal instances.

The configuration is stored within the folder `app`.

### config.admintool.json

This configuration file contains the basic configuration elements.

The effects of the configuration keys in the `config.admintool.json` is described in the following:

|Option|Description|Example|
|------|-----------|-------|
|`layerConf`|`services.json` file URL. All defined services will be available in *Masterportal Admin*. The file `config.masterportalApi.json` (see below) also refers to it.|`"http://geoportal-hamburg.de/lgv-config/services-internet.json"`|
|`restConf`|`rest-services.json` file URL. All defined REST services will be available in *Masterportal Admin*.|`"http://geoportal-hamburg.de/lgv-config/rest-services-internet.json"`|
|`styleConf`|`style.json` file URL. All defined styles will be available in *Masterportal Admin*.|`"http://geoportal-hamburg.de/lgv-config/style_v3.json"`|
|`hostedMasterportalURL`|A separately hosted *Masterportal* is required to preview and publish configurations. Enter the URL here.|`"https://141.91.163.211/stable/admin"`|
|`hostedMasterportalURLRouted`|URL of a *Masterportal* with enabled `mod-rewrite`. With this, more path features can be used. Defaults to `false`, indicating that `hostedMasterportalURL` is to be used.|
|`basePath`|Additional pathname. Used if *Masterportal Admin* isn't hosted within its domain's root. Example: Use `/masterportal-admin/` to offer *Masterportal Admin* at `http://localhost:9002/masterportal-admin/`. The given path should start and end with a slash. If not set, the application assumes to be running at root, in the example `http://localhost:9002/`. *Please note that changes to this key require re-building the frontend files!*|`"/nested/tool"`|
|`masterportalConfigJsonTemplateURL`|Definition of a template for a configuration of Masterportal. A template may be placed within the `app` folder and is in turn hosted by Masterportal Admin. In this case the filename should be provided as attribute to this key. A remote template can be referenced by the full url.|`masterportalConfig.json`or`https://example.com/configTemplate.json`|
|`https`|If `true`, all content is served via https.|`true`|
|`certificate`|Path to the certificate used for serving via https. If not defined a temporary certificate will be generated.|`"./certificate.crt"`|
|`privateKey`|Path to the private key of the certificate. If not defined, a temporary certificate will be generated.|`"./privateKey.key"`|
|`dbPath`|Path to sqlite database used to store masterportal configurations.|`"./configs.sqlite"`|
|`expertMode`|Configure if expert mode checkbox is shown.|`false`|
|`excludedChapters`|Sets chapters to be excluded from the admin tool e.g. to reduce the complexity of the configuration. Allowed values are listed in the table below.|`["portalconfig"]`|
|`excludedFeatures`|Sets tools and features to be excluded from the admin tool e.g. to reduce the complexity of the configuration. Allowed values are listed in the table below.|`["wfsSearch"]`|
|`renameMatrix`|This matrix sets labels that should be renamed. As an example this could be used to relabel the node `Themenconfig` in the GUI to "Topics and Maps".|`{"EN": {"Themenconfig": "Topics and Maps"}}`|
|`renameKeys`|This matrix sets labels that should be renamed. As an example, this could be used to relabel the node at path `Portalconfig.menu.tools.children` in the GUI to "Tools".|`{"EN": {"Portalconfig.menu.tools.children": "Tools"}}`|
|`sortAlphabetically`|Nodes that are set here will have their outgoing edges sorted by name in the GUI instead of their outgoing order. As an example this could be used to sort all the controls in `Portalconfig.controls` in an alphabetical order.|`["Portalconfig.controls"]`|
|`isSortable`|Nodes that are set here will have their outgoing edges sortable in the UI. As an example, this could be used to allow re-sorting the tools in `Portalconfig.menu.tools.children` in the resulting menu.|`["Portalconfig.menu.tools.children"]`|

Following values can be used in `excludedFeatures`:

| value | description |
| --- | ----------- |
|`"loadconfigfromfile"`| The "Upload from computer or internet" section in chapter "Choose initial configuration".|
|`"loadconfigfromtemplate"`| The "Use a template to create a configuration" section in chapter "Choose initial configuration".|
|`"loadconfigfromversion"`| The "Load a Masterportal version's default" section in chapter "Choose initial configuration".|
|`"loadjsonconfigfromurl"`| The "Load configuration from URL" section in chapters "Map services", "REST services" and "Styles".|
|`"loadjsonconfigfromfile"`| The "Upload local configuration file" section in chapters "Map services", "REST services" and "Styles".|
|`"exportpreview"`| The "Preview" section in chapter "Export".|
|`"exportpublish"`| The "Publish" section in chapter "Export".|
|`"exportdownloadfull"`| The "Download a configured Masterportal instance" section in chapter "Export".|
|`"exportdownloadjson"`| The "Download the central configuration file" section in chapter "Export".|
|`tool`| The name of the tool in the subchapters of "Portalconfig". E.g. `"wfsSearch"`.|

Following values can be used in `excludedChapters`:

| value | type | description |
| --- | ---- | ----------- |
|`"introduction"`| main chapter | Chapter "Choose global configuration" |
|`"loadconfig"`| main chapter | Chapter "Choose initial configuration" |
|`"portalconfig"`| main chapter | Chapter "Portalconfig" |
|`"themenconfig"`| main chapter | Chapter "Themenconfig" |
|`"export"`| main chapter | Chapter "Export" |
|`"layerconf"`| sub chapter | Chapter "Choose global configuration" > "Map services" |
|`"restconf"`| sub chapter | Chapter "Choose global configuration" > "REST services" |
|`"styleconf"`| sub chapter | Chapter "Choose global configuration" > "Styles" |
|`"themenconfig.groupconfig"`| sub chapter | Chapter "Themenconfig" > "Groupconfig" |
|`"themenconfig.layerconfig"`| sub chapter | Chapter "Themenconfig" > "Layerconfig" |
|`"themenconfig.styleconfig"`| sub chapter | Chapter "Themenconfig" > "Styleconfig" |
|`"themenconfig.gficonfig"`| sub chapter | Chapter "Themenconfig" > "Gficonfig" |
|`"portalconfig." + x`| sub chapter | Chapter "Portalconfig" > X. Where X must be substituted with the respective sub chapter. E.g. `"portalconfig.menu"`. See URL bar for additional values. |

The export function (that is, downloading a fully configured *Masterportal* as a zip file) requires the backend to fetch data from Bitbucket.
Should the export function not work, please check whether Node is able to retrieve information from there.
It may be required to configure a proxy with access to the Internet to do so.

### config.masterportalApi.json

This configuration file may be changed at any time, independent of build time.
It does *not* refer to the *Masterportal* itself, but to the map used within the *Masterportal Admin* to allow describing centers, extents, layer previews, and so on.
This feature uses the [masterportalAPI](https://bitbucket.org/geowerkstatt-hamburg/masterportalapi/src/master/).

It should usually suffice to edit `"startCenter"` and `"extent"` to match the position of expected *Masterportal Admin* users.
By default, values regarding Hamburg are provided.

Furthermore, you may provide the background map as `"layers"`.
These are **not** relevant for the configured *Masterportal*, but rather serve user orientation during configuration.

## Installation

There are two options for hosting *Masterportal Admin*:

1. [Local installation](#markdown-header-local-installation)
1. [Docker environment](#markdown-header-docker-environment)

### Local installation

#### Prerequisites

To run a hosted *Masterportal Admin* locally, these components are mandatory:

- [Node](https://nodejs.org) >= 8.12.0 (necessary for building the application and running the backend)
- *Masterportal Admin*.
  You may get the source code using Git with: `git clone --single-branch --branch master https://bitbucket.org/geowerkstatt-hamburg/mp-admintool.git`

It is recommended to install an instance of Masterportal.
The installation is described in the [installation guide of Masterportal](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/dev/doc/setup.md).
As an alternative, you may use an existing instance using the same 3J configuration files (`services.json`, `rest-services.json` and `style.json`).

#### Building

It is necessary to build the frontend before running *Masterportal Admin*.
The building process may be done on another machine and requires more dependencies.

```bash
NODE_ENVIRONMENT=development npm i
NODE_ENVIRONMENT=production npm run build
```

Re-building the frontend is mandatory on updates of the source code or on changes of the `basePath` [configuration](#markdown-header-configuration) option in `config.admintool.json`.

#### Running

If the frontend was built on another machine, it is necessary to install the runtime dependencies now:

```bash
NODE_ENVIRONMENT=production npm i
```

Otherwise, you may remove the build-time dependencies now by running:

```bash
NODE_ENVIRONMENT=production npm prune
```

The *Masterportal Admin* may then be started by the following command:

```bash
NODE_ENVIRONMENT=production npm run prod
```

We recommend using a process manager for a stable system that runs the *Masterportal Admin* after e.g. reboots.
For example, PM2 could be used.

On calling the endpoint `/clean`, all preview configurations older than 24 hours are deleted.
See description on the export page of *Masterportal Admin*.
Should this cleaning happen on a regular basis, we suggest making a CRON job call this URL in a time interval of your choice.

### Docker environment

The git repository provides a `Dockerfile` and a `docker-compose.yaml`.
You may use [Docker directly](#markdown-header-using-docker) to run *Masterportal Admin* or you may use [Docker Compose](#markdown-header-docker-compose).
Of course, Docker (and, in case of using Docker Compose, that one too) needs to be installed for following the further instructions.

In either case, you start by cloning the repository:

```bash
git clone --single-branch --branch master https://bitbucket.org/geowerkstatt-hamburg/mp-admintool.git
```

#### Using docker

For building a docker image, just build the image:

```bash
docker build --tag mpadmin .
```

If the container doesn't have direct access to the Internet, use the `HTTP_PROXY` and `HTTPS_PROXY` arguments:

```bash
docker build --tag mpadmin --build-arg HTTP_PROXY="http://127.0.0.1:3128" --build-arg HTTPS_PROXY="http://127.0.0.1:3128" .
```

If you want to use a base path, just adapt the `config.admintool.json` before building.

> 💡 Re-building the frontend is mandatory on updates of the source code or on changes of the `basePath` [configuration](#markdown-header-configuration) option in `config.admintool.json`.

For running the built docker image, run `docker run` like this:

```bash
docker run -p 9001:3000 mpadmin
```

The container is exposing port 3000.
Use `-p` to map it to a desired port on the host system.
In the example above, port 9001 will be exposed on the host system.

#### Using docker-compose

If necessary, you may configure proxy servers by adapting the configuration file named `.env`.
A template on how this configuration file might look like can be found in the repository as [.env](./.env).

If you want to use a base path, just adapt the `config.admintool.json` before building.

To build the container, use the following command:

```bash
docker-compose build
```

To start the container, you just start the container:

```bash
docker-compose up -d
```

You can set the (external) port to a value that fits your setup.
This may be achieved by changing the corresponding value in the `docker-compose.yaml`.
The external port defaults to port 9001.

The configurations are stored at `/mp-admintool/configs.sqlite` inside the container by default.
You may adapt this path by changing the `dbPath` key in the [config.admintool.json](#markdown-header-configadmintooljson).
By default, these configurations are lost at container restarts.

If you want to save these configurations permanent between container restarts, you can define a volume for that file.
Therefore, add the following to the `docker-compose.yaml`:

```yaml
services:
  mpadmin:
    [...]
    volumes:
      - mpadmin_db:/mp-admintool/configs.sqlite
volumes:
  mpadmin_db:
```

#### Deprecated Dockerfile

Up to version 1.0.0 of *Masterportal Admin*, an old version of the Dockerfile was used.
That version is shipped as `Dockerfile.git`.
It is, however, recommended to stick with the new `Dockerfile` and `docker-compose.yaml`, unless you have some good reason not to do so.

## Troubleshooting

### Configured masterportal-bundle zip file isn't provided

Masterportal Admin is a proxy for fetching the masterportal-bundle, as direct access isn't possible (CORS).
If Masterportal Admin runs behind a cooperate proxy and doesn't have direct access to the internet, this might fail in case of proxy misconfiguration.
In this case, please check if the proxy is defined within your local setup.
Also check if the proxy configuration of Docker is valid, if you are using Docker for *Masterportal Admin*.
You can do this by running `docker info`.
Please note that it's important to set the protocols, if you plan to use a http-proxy for encrypted connections!

### Masterportal Admin does not work in private mode

*IndexedDB* is used to store information about layers.
Browsers (e.g., Firefox) may deny the access to the database when running in private mode.
Hence, the tool may not work in private mode.

### Preview maps do not work

The *Masterportal Admin* offers input elements based on [masterportalAPI](https://bitbucket.org/geowerkstatt-hamburg/masterportalapi/src/master/) maps.
When changing `layerConf`, you may want/need to update the information that input element is using.
You can do so in the file `config.masterportalApi.json`, where base layer ID, initial center coordinates, and available extent can be defined.
